/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.1.36-community : Database - smart
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `smart`;

/*Table structure for table `activities` */

CREATE TABLE `activities` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(50) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL,
  `addeddate` date DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `activities` */

insert  into `activities`(`activity_id`,`activity_name`,`charge`,`addeddate`) values (5,'karataka',5000,'2015-03-09'),(6,'Aerobic Dance',15000,'2015-03-09'),(7,'Cricket',1000,'2015-03-09'),(8,'Aerobic Dance',1300,'2015-03-09'),(9,'Counting Jumps',5000,'2015-03-09'),(10,'Straight Jumping',7500,'2015-03-09'),(11,'Three Legged Race',4500,'2015-03-09'),(12,'Kickball',7000,'2015-03-09'),(13,'Water Games',13000,'2015-03-09'),(19,'Worm Farm',1500,'2015-03-09'),(20,'Cricket',1500,'2015-01-09'),(25,'Aerobic Dance',12000,'2015-03-09');

/*Table structure for table `admin_inbox_table` */

CREATE TABLE `admin_inbox_table` (
  `admin_inbox_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbox_message` varchar(500) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`admin_inbox_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `admin_inbox_table` */

insert  into `admin_inbox_table`(`admin_inbox_id`,`inbox_message`,`date`) values (2,'Mark the Attendance for the following: \nSender = Class Teacher\nSenderName = staffA\nSenderId = 16\nFrom Date = May-31-2013\nTo Date = May-27-2013',NULL),(3,'Mark the Attendance for the following: Staff Id = 1, Name = Srinivas - Srinivas, From Date = Nov-29-2013, To Date = Nov-30-2013',NULL),(4,'Mark the Attendance for the following: Parent Id = 1, Name = parentA, From Date = 03-09-2015, To Date = 03-10-2015',NULL),(5,'Mark the Attendance for the following: Staff Id = 1, Name = Srinivas - Srinivas, From Date = Mar-12-2015, To Date = Mar-13-2015',NULL);

/*Table structure for table `admin_info_table` */

CREATE TABLE `admin_info_table` (
  `admin_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_user_name` varchar(20) DEFAULT NULL,
  `admin_password` varchar(20) DEFAULT NULL,
  `admin_name` varchar(20) DEFAULT NULL,
  `admin_gender` varchar(15) DEFAULT NULL,
  `admin_address` varchar(200) DEFAULT NULL,
  `admin_phone_number` varchar(15) DEFAULT NULL,
  `admin_dynamic_password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`admin_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `admin_info_table` */

insert  into `admin_info_table`(`admin_info_id`,`admin_user_name`,`admin_password`,`admin_name`,`admin_gender`,`admin_address`,`admin_phone_number`,`admin_dynamic_password`) values (1,'administrator','administrator','administrator','Male','Bangalore','8722202200','p(4DY{f94w_~]h}6'),(2,'admin','admin','admin','male','Banglore','800936517','8y@k7.jd.-p_:BPj'),(3,'masteradmin','masteradmin','SANTOSH SAHU','Male','Bangalore','9008365317',';7Q(Xb/Qb/WD}p%Z');

/*Table structure for table `admin_photos_table` */

CREATE TABLE `admin_photos_table` (
  `admin_photo_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` mediumtext,
  PRIMARY KEY (`admin_photo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `admin_photos_table` */

insert  into `admin_photos_table`(`admin_photo_id`,`image`) values (1,'banner.png'),(2,'Desert.jpg'),(3,'Desert.jpg'),(4,'Tulips.jpg'),(5,'Desert.jpg'),(6,'Koala.jpg');

/*Table structure for table `announcement_table` */

CREATE TABLE `announcement_table` (
  `announcement_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `announcement` varchar(500) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `staff` tinyint(1) DEFAULT NULL,
  `parent` tinyint(1) DEFAULT NULL,
  `student` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`announcement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `announcement_table` */

insert  into `announcement_table`(`announcement_id`,`title`,`announcement`,`date`,`staff`,`parent`,`student`) values (2,'Good Friday Celebration','Test Has been cancled due to good Friday','2013-11-09 00:00:00',1,1,1),(4,'Please Collect your Id cards as soon as possible before 1st Auguest','HI THESE IS TO INFORM THAT  TOMMAROW IS HOLIDAY','2013-11-11 00:00:00',1,1,1),(5,'Lakshimi Puja Holiday','HI THESE IS TO INFORM THAT  TOMMAROW IS HOLIDAY','2013-11-13 00:00:00',1,0,1),(6,'Lakshimi Puja Holiday','HI THESE IS TO INFORM THAT  TOMMAROW IS HOLIDAY','2013-12-20 00:00:00',1,1,1),(7,'Good Friday Celebration','HI THESE IS TO INFORM THAT  TOMMAROW IS HOLIDAY','2013-12-24 00:00:00',1,0,1),(8,'Good Friday Celebration','HI this is santosh here  inform abou the lakshmi puja','2015-02-08 00:00:00',1,1,1),(9,'Lakshimi Puja Holiday','HI this is santosh here due to lakshmi puja school remain closed for all the days','2015-02-08 00:00:00',1,1,1),(10,'Lakshimi Puja Holiday','HI this is santosh here due to lakshmi puja school remain closed for all the days','2015-02-08 00:00:00',1,1,1),(11,'Lakshimi Puja Holiday','HI this is santosh here due to lakshmi puja school remain closed for all the days','2015-02-08 00:00:00',1,1,1),(12,'Lakshimi Puja Holiday','HI this is santosh here due to lakshmi puja school remain closed for all the days','2015-02-08 00:00:00',1,1,1),(13,'Lakshimi Puja Holiday','HI this is santosh here due to lakshmi puja school remain closed for all the days','2015-02-08 00:00:00',1,1,1);

/*Table structure for table `authentication_table` */

CREATE TABLE `authentication_table` (
  `authentication_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Parent_FK` bigint(20) DEFAULT NULL,
  `staff_FK` bigint(20) DEFAULT NULL,
  `student_FK` bigint(20) DEFAULT NULL,
  `admin_FK` bigint(20) DEFAULT NULL,
  `admin_Class` tinyint(1) DEFAULT NULL,
  `admin_Subject` tinyint(1) DEFAULT NULL,
  `admin_NoticeBoard` tinyint(1) DEFAULT NULL,
  `admin_Student` tinyint(1) DEFAULT NULL,
  `admin_Parent` tinyint(1) DEFAULT NULL,
  `admin_staff` tinyint(1) DEFAULT NULL,
  `admin_Attandance` tinyint(1) DEFAULT NULL,
  `admin_MarkAnalysis` tinyint(1) DEFAULT NULL,
  `admin_Exam` tinyint(1) DEFAULT NULL,
  `admin_ExtraActivities` tinyint(1) DEFAULT NULL,
  `admin_Library` tinyint(1) DEFAULT NULL,
  `admin_TimeTable` tinyint(1) DEFAULT NULL,
  `admin_Transport` tinyint(1) DEFAULT NULL,
  `admin_Fees` tinyint(1) DEFAULT NULL,
  `admin_Admission` tinyint(1) DEFAULT NULL,
  `admin_Report` tinyint(1) DEFAULT NULL,
  `admin_Settings` tinyint(1) DEFAULT NULL,
  `parent_mailBox` tinyint(1) DEFAULT NULL,
  `parent_attandance` tinyint(1) DEFAULT NULL,
  `parent_markAnalysis` tinyint(1) DEFAULT NULL,
  `parent_changePassowrd` tinyint(1) DEFAULT NULL,
  `parent_portfolio` tinyint(1) DEFAULT NULL,
  `parent_ExtraActivities` tinyint(1) DEFAULT NULL,
  `parent_News` tinyint(1) DEFAULT NULL,
  `parent_StudentGraphs` tinyint(1) DEFAULT NULL,
  `student_MailBox` tinyint(1) DEFAULT NULL,
  `student_Scheduler` tinyint(1) DEFAULT NULL,
  `student_attandance` tinyint(1) DEFAULT NULL,
  `student_MarkAnalysis` tinyint(1) DEFAULT NULL,
  `student_ChangePassword` tinyint(1) DEFAULT NULL,
  `student_Quize` tinyint(1) DEFAULT NULL,
  `student_Graph` tinyint(1) DEFAULT NULL,
  `student_News` tinyint(1) DEFAULT NULL,
  `student_PortFolio` tinyint(1) DEFAULT NULL,
  `student_TimeTable` tinyint(1) DEFAULT NULL,
  `staff_Home` tinyint(1) DEFAULT NULL,
  `staff_MailBox` tinyint(1) DEFAULT NULL,
  `staff_PhotoGallary` tinyint(1) DEFAULT NULL,
  `staff_Attandance` tinyint(1) DEFAULT NULL,
  `staff_MarkList` tinyint(1) DEFAULT NULL,
  `staff_LeaveApplication` tinyint(1) DEFAULT NULL,
  `staff_Change Password` tinyint(1) DEFAULT NULL,
  `staff_pointBucket` tinyint(1) DEFAULT NULL,
  `staff_HomeWork` tinyint(1) DEFAULT NULL,
  `staff_UserName` varchar(50) DEFAULT NULL,
  `staff_Password` varchar(50) DEFAULT NULL,
  `student_UserName` varchar(50) DEFAULT NULL,
  `student_Password` varchar(50) DEFAULT NULL,
  `admin_UserName` varchar(50) DEFAULT NULL,
  `admin_Password` varchar(50) DEFAULT NULL,
  `parent_UserName` varchar(50) DEFAULT NULL,
  `Parent_Password` varchar(50) DEFAULT NULL,
  `Privilage` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`),
  KEY `FKB54EC7473210A4D5` (`student_FK`),
  KEY `FKB54EC747419B6609` (`Parent_FK`),
  KEY `FKB54EC747B8D854CB` (`staff_FK`),
  KEY `FKB54EC74724CC566D` (`admin_FK`),
  CONSTRAINT `authentication_table_ibfk_1` FOREIGN KEY (`Parent_FK`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `authentication_table_ibfk_2` FOREIGN KEY (`staff_FK`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `authentication_table_ibfk_3` FOREIGN KEY (`student_FK`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `authentication_table_ibfk_4` FOREIGN KEY (`admin_FK`) REFERENCES `admin_info_table` (`admin_info_id`),
  CONSTRAINT `FKB54EC74724CC566D` FOREIGN KEY (`admin_FK`) REFERENCES `admin_info_table` (`admin_info_id`),
  CONSTRAINT `FKB54EC7473210A4D5` FOREIGN KEY (`student_FK`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FKB54EC747419B6609` FOREIGN KEY (`Parent_FK`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `FKB54EC747B8D854CB` FOREIGN KEY (`staff_FK`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

/*Data for the table `authentication_table` */

insert  into `authentication_table`(`authentication_id`,`Parent_FK`,`staff_FK`,`student_FK`,`admin_FK`,`admin_Class`,`admin_Subject`,`admin_NoticeBoard`,`admin_Student`,`admin_Parent`,`admin_staff`,`admin_Attandance`,`admin_MarkAnalysis`,`admin_Exam`,`admin_ExtraActivities`,`admin_Library`,`admin_TimeTable`,`admin_Transport`,`admin_Fees`,`admin_Admission`,`admin_Report`,`admin_Settings`,`parent_mailBox`,`parent_attandance`,`parent_markAnalysis`,`parent_changePassowrd`,`parent_portfolio`,`parent_ExtraActivities`,`parent_News`,`parent_StudentGraphs`,`student_MailBox`,`student_Scheduler`,`student_attandance`,`student_MarkAnalysis`,`student_ChangePassword`,`student_Quize`,`student_Graph`,`student_News`,`student_PortFolio`,`student_TimeTable`,`staff_Home`,`staff_MailBox`,`staff_PhotoGallary`,`staff_Attandance`,`staff_MarkList`,`staff_LeaveApplication`,`staff_Change Password`,`staff_pointBucket`,`staff_HomeWork`,`staff_UserName`,`staff_Password`,`student_UserName`,`student_Password`,`admin_UserName`,`admin_Password`,`parent_UserName`,`Parent_Password`,`Privilage`) values (76,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1,1,1,1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(77,NULL,NULL,NULL,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `book_author` */

CREATE TABLE `book_author` (
  `slno` bigint(20) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(40) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `book_author` */

insert  into `book_author`(`slno`,`author_name`,`description`) values (5,'Nancy Buckingham','Nancy J. Buckingham Sawyer (born 10 August 1924) is a British writer who co-authored over 45 gothic and romance novels in collaboration with her husband, John Sawyer (4 October 1919 - 1992).'),(6,'Hans Aanrud ','Hans Aanrud (3 September 1863 – 11 January 1953) was a Norwegian author. He wrote plays, poetry, and stories depicting rural life in his native Gudbrandsdal, Norway.'),(7,'Alexander Aaronsohn',' (Hebrew: Romania, 1888–Palestine, 1948) was an author and activist who wrote about the plight of people living in Palestine (now Israel) in his book, With the Turks in Palestine.'),(8,'Chris Abani','Christopher \"Chris\" Abani (born 27 December 1966) is a Nigerian author. He is part of a new generation of Nigerian writers working to convey to an English-speaking audience the experience of those born and raised in \"that troubled African nation\".');

/*Table structure for table `book_category` */

CREATE TABLE `book_category` (
  `slno` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_category_name` varchar(40) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `book_category` */

insert  into `book_category`(`slno`,`book_category_name`,`description`) values (14,'Biology','The science of life and of living orga'),(15,'Social ','. The science of life and of living organisms, including their structure, function, growth, origin, evolution, and distribution. '),(16,'Architecture','the art or science of designing and creating '),(17,'Business',' An organization or enterprising entity engaged in commercial, industrial or professional activities. .'),(18,'Fiction','In 1701 young Lord Asano is goaded into attacking a corrupt official at the Japanese Court. Although the wound Asano inflicts is minimal, the Emperor’s . '),(19,'Medical audiobooks','Longlisted for both the Guardian First Book Award and the Samuel Johnson Prize for Non-Fiction, DO NO HARM ranks alongside '),(20,'Fun & Games ',''),(22,'Performing Arts ',''),(25,'Humor ','About this book: On January 12, 2010, a major earthquake struck near Port-au-Prince, Haiti. Hundreds of thousands of people died, and the greater...more');

/*Table structure for table `book_issue_table` */

CREATE TABLE `book_issue_table` (
  `book_issue_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_id` bigint(20) DEFAULT NULL,
  `book_table_id` bigint(20) DEFAULT NULL,
  `book_issue_date` date NOT NULL,
  `book_receive_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `fine_per_day` int(11) NOT NULL,
  `exceed_days` bigint(20) DEFAULT NULL,
  `total_fine` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`book_issue_id`),
  KEY `FK35C904923210A52B` (`student_id`),
  KEY `FK35C9049256113440` (`book_table_id`),
  KEY `FK35C90492964089D0` (`student_id`),
  KEY `FK35C904924F3F0F3B` (`book_table_id`),
  CONSTRAINT `FK35C904923210A52B` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK35C904924F3F0F3B` FOREIGN KEY (`book_table_id`) REFERENCES `book_table` (`book_table_id`),
  CONSTRAINT `FK35C9049256113440` FOREIGN KEY (`book_table_id`) REFERENCES `book_table` (`book_table_id`),
  CONSTRAINT `FK35C90492964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `book_issue_table` */

insert  into `book_issue_table`(`book_issue_id`,`student_id`,`book_table_id`,`book_issue_date`,`book_receive_date`,`status`,`fine_per_day`,`exceed_days`,`total_fine`) values (1,2,2,'2014-01-03','2014-01-15',1,100,NULL,NULL),(2,2,2,'2015-03-20','2015-03-28',1,100,5,500);

/*Table structure for table `book_location` */

CREATE TABLE `book_location` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `book_location_name` varchar(40) DEFAULT NULL,
  `book_location_description` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `book_location` */

insert  into `book_location`(`slno`,`book_location_name`,`book_location_description`) values (3,'Rack C','Rack C'),(4,'Rack Second','Rack B'),(5,'Rack c','Rack c'),(6,'Rack D','This is 4th Rack to Store Books');

/*Table structure for table `book_publisher_details` */

CREATE TABLE `book_publisher_details` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `book_publisher_name` varchar(40) DEFAULT NULL,
  `book_publisher_address` varchar(40) DEFAULT NULL,
  `book_publisher_city` varchar(40) DEFAULT NULL,
  `book_publisher_state` varchar(40) DEFAULT NULL,
  `book_publisher_zipcode` bigint(40) DEFAULT NULL,
  `book_publisher_mobileno` bigint(40) DEFAULT NULL,
  `book_publishser_telephoneno` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `book_publisher_details` */

insert  into `book_publisher_details`(`slno`,`book_publisher_name`,`book_publisher_address`,`book_publisher_city`,`book_publisher_state`,`book_publisher_zipcode`,`book_publisher_mobileno`,`book_publishser_telephoneno`) values (2,'jagdish','Bangalore','Bangalore','karnataka',560102,9008365317,'080-32135415'),(4,'jagdish','Bangalore','Bangalore','karnataka',560102,9008365317,'080-32135415'),(10,'India Book House','Bangalore','Bangalore','karnataka',560102,9008365317,'0322256001');

/*Table structure for table `book_purchase_details` */

CREATE TABLE `book_purchase_details` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(40) DEFAULT NULL,
  `purchased_date` date DEFAULT NULL,
  `vendor_name` varchar(40) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  PRIMARY KEY (`slno`),
  UNIQUE KEY `book_id` (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `book_purchase_details` */

insert  into `book_purchase_details`(`slno`,`book_id`,`purchased_date`,`vendor_name`,`quantity`,`price`) values (8,'b123','2015-04-02','Santosh',20,123),(9,'b222','2015-04-10','Jagdish',12,123),(10,'B129','2015-05-27','Jagdish',10,120),(11,'B115','2015-09-15','Jagdish',10,250);

/*Table structure for table `book_register` */

CREATE TABLE `book_register` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(40) DEFAULT NULL,
  `reference_id` varchar(40) DEFAULT NULL,
  `title` varchar(40) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  `book_category_name` varchar(40) DEFAULT NULL,
  `author_name` varchar(40) DEFAULT NULL,
  `publisher_name` varchar(40) DEFAULT NULL,
  `published_year` varchar(40) DEFAULT NULL,
  `is_reference_book` tinyint(1) DEFAULT NULL,
  `language` varchar(40) DEFAULT NULL,
  `book_location` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `book_register` */

insert  into `book_register`(`slno`,`book_id`,`reference_id`,`title`,`description`,`book_category_name`,`author_name`,`publisher_name`,`published_year`,`is_reference_book`,`language`,`book_location`) values (25,'b123','123','Java','asd','Sceince','santosh kiran','Santosh','2012',1,'Java','Rack First'),(26,'b222','222','C','C description','Social','David Sne','Santosh','2012',1,'C Language','Rack First'),(27,'b111','111','Oracle','Oracle','Biology','santosh kiran','Jagdish','2012',1,'Enlish','Rack C'),(28,'B129','R129','Computer General','Fundamentals of Computer Programming','Biology','Nancy Buckingham','ABP Group','2010',1,'English','Rack c'),(29,'B115','REF123','JAVA','JAVA ARCHITECTURE','Business','Nancy Buckingham','India Book House','2009',1,'ENGLISH','Rack C');

/*Table structure for table `book_table` */

CREATE TABLE `book_table` (
  `book_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_author` varchar(255) DEFAULT NULL,
  `book_name` varchar(255) DEFAULT NULL,
  `book_number` varchar(255) DEFAULT NULL,
  `book_edition_no` varchar(255) DEFAULT NULL,
  `book_date` date DEFAULT NULL,
  `book_price` bigint(20) DEFAULT NULL,
  `number_of_copies` bigint(20) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `book_storage_hint` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`book_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `book_table` */

insert  into `book_table`(`book_table_id`,`book_author`,`book_name`,`book_number`,`book_edition_no`,`book_date`,`book_price`,`number_of_copies`,`category`,`book_storage_hint`) values (2,'Dr. Sushila Madan','Computer Fundamentals','CS001','2009','2015-03-20',200,10,'ComputerScience','This Books Belong to Sharma and Sharma Groups '),(3,'Anita Goel','Computer Fundamentals','CS002','7 impression, 2014','2015-03-20',250,20,'ComputerScience',NULL),(4,'Reema Thareja','Computer Fundamentals and Programming in C','CS003',' 	2012 Reprint, 2013','2015-03-20',150,30,'ComputerScience',NULL),(5,' 	Prof. Satish Jain, Shashank Jain, Shashi Singh & M. Geetha Iyer','\'O\' Level Made Simple IT Tools and Business Systems (Based on DOEACC \'O\' Level)','CS004','4th Edition,Latest Reprint ','2015-03-20',450,10,'ComputerScience',NULL),(6,'Prof. Satish Jain, Shashank Jain, Shashi Singh & M. Geetha Iyer',' 	\'A\' Level Made Simple Structured Systems Analysis and Design (Based on DOEACC \'A\' Level)','CS005','2010','2015-03-20',500,5,'ComputerScience',NULL),(7,' 	ASAP WORLD','Administering SAP R/3: SD-Sales Distribution Module','CS006','2012','2015-03-20',100,30,'SAP',NULL),(8,' 	ASAP WORLD',' 	Administering SAP R/3: The FI-Finacial & Co - Controlling Modules','CS007','2012','2015-03-20',200,50,'SAP',NULL),(9,' 	Buck Woody','Administrator\'s Guide to SQL Server 2005 : Authoritative Guides for Microsoft Windows and Server Professionals','CS008','2007','2015-03-20',100,20,'SAP',NULL),(10,'TED PADOVA','ADOBE ACROBAT X PDF BIBLE (BIBLE series)','CS009',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `book_vendor` */

CREATE TABLE `book_vendor` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `book_vendor_name` varchar(40) DEFAULT NULL,
  `book_vendor_address` varchar(40) DEFAULT NULL,
  `book_vendor_city` varchar(40) DEFAULT NULL,
  `book_vendor_state` varchar(40) DEFAULT NULL,
  `book_vendor_zipcode` varchar(40) DEFAULT NULL,
  `book_vendor_mobileno` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `book_vendor` */

insert  into `book_vendor`(`slno`,`book_vendor_name`,`book_vendor_address`,`book_vendor_city`,`book_vendor_state`,`book_vendor_zipcode`,`book_vendor_mobileno`) values (43,'Santosh','Bangalore','Bangalore','Karnataka','560010','9008610014'),(44,'Jagdish','Bangalore','Bangalore','Karnataka','560010','8095280900'),(45,'raj','HSR Layout','bangalore','karnataka','560102','9008365317'),(46,'Mahi','bangalore','bangalore','karnataka','560102','9008365317'),(47,'Mahi','bangalore','bangalore','karnataka','560102','9008365317');

/*Table structure for table `class_subjects_table` */

CREATE TABLE `class_subjects_table` (
  `class_subjects_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject1` varchar(40) DEFAULT NULL,
  `subject2` varchar(40) DEFAULT NULL,
  `subject3` varchar(40) DEFAULT NULL,
  `subject4` varchar(40) DEFAULT NULL,
  `subject5` varchar(40) DEFAULT NULL,
  `subject6` varchar(40) DEFAULT NULL,
  `subject7` varchar(40) DEFAULT NULL,
  `subject8` varchar(40) DEFAULT NULL,
  `subject9` varchar(40) DEFAULT NULL,
  `subject10` varchar(40) DEFAULT NULL,
  `subject11` varchar(40) DEFAULT NULL,
  `subject12` varchar(40) DEFAULT NULL,
  `subject13` varchar(40) DEFAULT NULL,
  `subject14` varchar(40) DEFAULT NULL,
  `subject15` varchar(40) DEFAULT NULL,
  `class_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`class_subjects_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

/*Data for the table `class_subjects_table` */

insert  into `class_subjects_table`(`class_subjects_id`,`subject1`,`subject2`,`subject3`,`subject4`,`subject5`,`subject6`,`subject7`,`subject8`,`subject9`,`subject10`,`subject11`,`subject12`,`subject13`,`subject14`,`subject15`,`class_name`) values (48,'Moral Science','Political Science','English','Geography','History','G.K','Computer','MoralScience','Grammer','Hindi','History','Maths','Maths2','Hindi','Chemistry','Class III'),(49,'Science','Physic','Chemistry','Hindi','Biology','Geog','Computer','MoralScience','English','Hindi','Geography','Maths','Maths2','Kannada','Geography','Class I'),(50,'Maths','History','Geography','MoralScience','Science','Hindi','Hindi','MoralScience','C++ ','Operating System',NULL,NULL,NULL,NULL,NULL,'Class II'),(51,'Hindi','Geography','MoralScience','Maths','Science','Hindi','Biology','MoralScience','Java','JSF',NULL,NULL,NULL,NULL,NULL,'Class V'),(52,'Cobol','Data Structure','Networking','Dbms','Operating System','Java 8.1','Jsf 2.2.3','Hibernate 4.0','Web services','Spring 3.0','Spring Security','JMS',NULL,NULL,NULL,'MCA'),(53,'Financial Accounting','Economics','Business Ethics','Marketing Management','Entrepreneurship','Strategic leadership','Negotiation','Operations Management','Marketing ','Corporate Finance',NULL,NULL,NULL,NULL,NULL,'MBA'),(54,'Communication Skill','DS','Computer Fundamental','Software Engineering','GUI Programming','Advance Database Management System','Object Oriented Programming','Relational Database Management Systems','Computer Networking',' SYSTEM DEVELOPMENT PROJECT',NULL,NULL,NULL,NULL,NULL,'BCA'),(55,'GEOMETRY','Social Science','Science','Mathematics','Hindi','English','Environmental Studies','Physics','Computer Science','Chemistry','Botany & Zoology',NULL,NULL,NULL,NULL,'Class IV'),(56,'Maths','Science','Geography','History','Civics','English Grammer','WrittenSkill','Algebra','Biology','Chemistry',NULL,NULL,NULL,NULL,NULL,'Class VII'),(57,'Bengali 1st Language','(English 1st Language','English 2nd Language','History','Geography','Physical Science','Life Science','Mathematics','Computer Networking','JSF2.2.3',NULL,NULL,NULL,NULL,NULL,'Class X'),(58,'Kannada','English','Physical Science','Life Science','History','Geography','Mathematics','Social Science','Botany & Zoology','Chemistry',NULL,NULL,NULL,NULL,NULL,'Class VI'),(59,'Hindi','History','Paribesh Porichoy','Geography','political Science','Physical Science','English Grammar','Computer Application','Physical Education','Work Education','2nd Language: English',NULL,NULL,NULL,NULL,'Class VIII'),(60,'English 1st Language','English 2nd Language','Hindi 1st Language','Hindi 2st Language','History','Geography','Physical Science','Life Science','Mathematics','Chemistry',NULL,NULL,NULL,NULL,NULL,'Class IX'),(61,'Accountancy','Business Studies','Computer Science','Economics','Mathematics','Hindi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Class XI');

/*Table structure for table `class_table` */

CREATE TABLE `class_table` (
  `class_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(50) DEFAULT NULL,
  `section_1` varchar(10) DEFAULT NULL,
  `section_2` varchar(10) DEFAULT NULL,
  `section_3` varchar(10) DEFAULT NULL,
  `section_4` varchar(10) DEFAULT NULL,
  `section_5` varchar(10) DEFAULT NULL,
  `section_6` varchar(10) DEFAULT NULL,
  `year_fee` int(11) DEFAULT NULL,
  PRIMARY KEY (`class_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

/*Data for the table `class_table` */

insert  into `class_table`(`class_table_id`,`class_name`,`section_1`,`section_2`,`section_3`,`section_4`,`section_5`,`section_6`,`year_fee`) values (13,'Class I','A','B','C','D','F','H',200000),(14,'Class II','A','B','C','D','E','F',75000),(69,'Class III','A','B','C','D','E','F',75000),(70,'Class V','A','B','C','D','E','F',120000),(71,'MBA','A','B','C','D','E','F',300000),(72,'Class VII','A','B','C','E','D','F',120000),(73,'Class IV','A','B','C','D','E','F',150000),(76,'MCA','A','B','C','D','E','F',150000),(78,'Class X','B','C','D','E','F','G',120000),(79,'Class VI','A','B','C','D','E','F',150000),(80,'Class VIII','A','B','C','D','E','F',180000),(81,'Class IX','A','B','C','D','E','F',150000),(82,'BCA','A','B','C','D','E','F',300000),(83,'Class XI','A','B','C','D','E','F',120000),(84,'Class XII','A','B','C','D','E','F',150000),(85,'MCA','A','B','C','D','E','F',3000000);

/*Table structure for table `designation_table` */

CREATE TABLE `designation_table` (
  `designation_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `designation_Name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`designation_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `designation_table` */

insert  into `designation_table`(`designation_table_id`,`designation_Name`) values (2,'Vice Principal'),(4,'Class Teacher'),(5,'Librarian'),(6,'principal'),(7,'peon'),(10,'Associate Software'),(11,'Software'),(14,'santosh'),(15,'jagdish'),(17,'Maintenance ');

/*Table structure for table `driver_info_table` */

CREATE TABLE `driver_info_table` (
  `driver_info_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `driver_name` varchar(20) DEFAULT NULL,
  `driver_id` varchar(20) DEFAULT NULL,
  `driver_contact_number` varchar(15) DEFAULT NULL,
  `driver_address` varchar(50) DEFAULT NULL,
  `driver_email_id` varchar(50) DEFAULT NULL,
  `driver_certificate` mediumtext,
  PRIMARY KEY (`driver_info_table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `driver_info_table` */

/*Table structure for table `empdata` */

CREATE TABLE `empdata` (
  `slno` int(11) NOT NULL AUTO_INCREMENT,
  `empid` varchar(20) DEFAULT NULL,
  `empname` varchar(20) DEFAULT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `education` varchar(20) DEFAULT NULL,
  `phno` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `pass` varchar(20) DEFAULT NULL,
  `repass` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `empdata` */

insert  into `empdata`(`slno`,`empid`,`empname`,`designation`,`education`,`phno`,`email`,`pass`,`repass`) values (1,'A1','Anil','sales','SSLC','234323432','A1','123','123'),(2,'A2','sunil','sales','PUC','234999999','A2','111','111'),(4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `exams_table` */

CREATE TABLE `exams_table` (
  `exams_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `exam_Name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`exams_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `exams_table` */

insert  into `exams_table`(`exams_table_id`,`exam_Name`) values (1,'First Term'),(2,'Second Term'),(3,'Quaterly'),(4,'HalfYearly'),(6,'Annual'),(11,'MidTerm'),(18,'screeningTest'),(19,'First Semester');

/*Table structure for table `expense_type` */

CREATE TABLE `expense_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `expense_type` varchar(40) DEFAULT NULL,
  `expense_description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`expense_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `expense_type` */

insert  into `expense_type`(`id`,`expense_type`,`expense_description`) values (1,'Travelling','Travelling expense'),(2,'Stationary','Stationary Expense'),(3,'Cleaning ','Cleaning Expense'),(4,'Food','Food Expense'),(5,'Misscelaneous','Extra Expense');

/*Table structure for table `fee_applied_to_certain_class` */

CREATE TABLE `fee_applied_to_certain_class` (
  `slno` bigint(40) unsigned NOT NULL AUTO_INCREMENT,
  `fee_occurence` varchar(40) DEFAULT NULL,
  `fee_category` varchar(40) DEFAULT NULL,
  `fee_amount` bigint(40) unsigned NOT NULL,
  `ukg_flag` tinyint(1) DEFAULT NULL,
  `lkg_flag` tinyint(1) DEFAULT NULL,
  `nursery` tinyint(1) DEFAULT NULL,
  `grade1_flag` tinyint(1) DEFAULT NULL,
  `grade2_flag` tinyint(1) DEFAULT NULL,
  `grade3_flag` tinyint(1) DEFAULT NULL,
  `grade4_flag` tinyint(1) DEFAULT NULL,
  `grade5_flag` tinyint(1) DEFAULT NULL,
  `grade6_flag` tinyint(1) DEFAULT NULL,
  `grade7_flag` tinyint(1) DEFAULT NULL,
  `grade8_flag` tinyint(1) DEFAULT NULL,
  `grade9_flag` tinyint(1) DEFAULT NULL,
  `grade10_flag` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `fee_applied_to_certain_class` */

insert  into `fee_applied_to_certain_class`(`slno`,`fee_occurence`,`fee_category`,`fee_amount`,`ukg_flag`,`lkg_flag`,`nursery`,`grade1_flag`,`grade2_flag`,`grade3_flag`,`grade4_flag`,`grade5_flag`,`grade6_flag`,`grade7_flag`,`grade8_flag`,`grade9_flag`,`grade10_flag`) values (1,'yearly','building fee',1200,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,'onlyonce','donation',5000,1,1,1,1,1,1,1,1,1,1,1,1,1),(3,'monthly','tuition fee',500,1,1,1,1,1,1,1,1,1,1,1,1,1),(4,'quarterly','term Fee',600,1,1,1,1,1,1,1,1,1,1,1,1,1),(5,'half yearly','semester fee',3000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'monthly','trip fee',500,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'MONTHLY','TUITION FEE',12000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'YEARLY','BUILDING FEE',18000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `fee_details` */

CREATE TABLE `fee_details` (
  `fee_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_id` bigint(20) DEFAULT NULL,
  `student_fee` int(11) DEFAULT NULL,
  `payee_name` varchar(50) DEFAULT NULL,
  `total_amount` int(11) DEFAULT NULL,
  `amount_paid` int(11) DEFAULT NULL,
  `due_amount` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `relation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`fee_detail_id`),
  KEY `FK1C813E893210A52B` (`student_id`),
  KEY `FK1C813E89964089D0` (`student_id`),
  CONSTRAINT `fee_details_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK1C813E89964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `fee_details` */

insert  into `fee_details`(`fee_detail_id`,`student_id`,`student_fee`,`payee_name`,`total_amount`,`amount_paid`,`due_amount`,`date`,`relation`) values (1,NULL,20000,'prathima',21005,11000,10005,'2014-02-13','sister'),(6,NULL,20000,'khader',20000,20000,0,'2014-01-02','Brother'),(7,NULL,25000,'basha',27000,26000,1000,'2014-01-08','brother');

/*Table structure for table `fee_paid_details` */

CREATE TABLE `fee_paid_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(40) DEFAULT NULL,
  `student_class` varchar(40) DEFAULT NULL,
  `student_section` varchar(40) DEFAULT NULL,
  `paid_total_fee` bigint(40) DEFAULT NULL,
  `remaining_fee` bigint(40) DEFAULT NULL,
  `fee_paid_date` date DEFAULT NULL,
  `student_id_fk` bigint(20) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC172B7E86FD00D66` (`student_id_fk`),
  KEY `FKC172B7E84C51BAF1` (`student_id_fk`),
  CONSTRAINT `fee_paid_details_ibfk_1` FOREIGN KEY (`student_id_fk`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FKC172B7E84C51BAF1` FOREIGN KEY (`student_id_fk`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `fee_paid_details` */

insert  into `fee_paid_details`(`id`,`student_name`,`student_class`,`student_section`,`paid_total_fee`,`remaining_fee`,`fee_paid_date`,`student_id_fk`,`student_id`) values (1,'Mohan Kumar','Nursery','A',10000,800,'2015-03-30',5,NULL),(2,'Mohan Kumar','Nursery','A',200,600,'2015-03-30',9,NULL),(3,'Mohan Kumar','Nursery','A',600,0,'2015-03-30',2,NULL),(4,'R.Santosh','Class I','A',8000,2800,'2015-03-31',8,NULL),(5,'Mohan Kumar','Class I','A',10800,0,'2015-03-31',2,NULL),(6,'R.Santosh','Class I','B',2800,0,'2015-03-31',8,NULL),(7,'Santosh','Class I','A',2000,8800,'2015-04-01',9,NULL),(8,'Santosh','Class I','A',800,8000,'2015-04-01',9,NULL),(9,'Harish','Class II','A',0,0,'2015-06-08',4,NULL),(10,'KHADER','Class I','B',22800,0,'2015-06-09',3,NULL),(11,'T.Santosh','Class VII','D',22800,0,'2015-06-09',6,NULL),(12,'SANTOSH','Class I','B',7000,1000,'2015-07-02',9,NULL),(13,'R.Santosh','Class I','A',15000,-15000,'2015-07-02',8,NULL);

/*Table structure for table `fee_structure_setup` */

CREATE TABLE `fee_structure_setup` (
  `slno` bigint(40) unsigned NOT NULL AUTO_INCREMENT,
  `fee_occurence` varchar(40) DEFAULT NULL,
  `fee_category` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `fee_structure_setup` */

insert  into `fee_structure_setup`(`slno`,`fee_occurence`,`fee_category`) values (1,'yearly','building fee'),(2,'monthly','tuition fee'),(3,'onlyonce','donation'),(4,'quarterly','Term Fee'),(8,'half yearly','semester fee'),(9,'monthly','trip fee'),(10,'yearly','');

/*Table structure for table `fine_collection_details` */

CREATE TABLE `fine_collection_details` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(40) DEFAULT NULL,
  `student_class` varchar(40) DEFAULT NULL,
  `student_section` varchar(40) DEFAULT NULL,
  `book_id` varchar(40) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `no_of_days_diff` bigint(40) DEFAULT NULL,
  `fine_amount` bigint(40) DEFAULT NULL,
  PRIMARY KEY (`slno`),
  KEY `FK5DB6BC46D3E6654` (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

/*Data for the table `fine_collection_details` */

insert  into `fine_collection_details`(`slno`,`student_name`,`student_class`,`student_section`,`book_id`,`due_date`,`no_of_days_diff`,`fine_amount`) values (67,'Santosh','Class I','A','b123','2015-04-21',5,20),(68,'Mohan Kumar','Class I','A','b123','2015-04-23',3,6),(69,'R.Santosh','Class I','A','b123','2015-04-20',6,12);

/*Table structure for table `general_infomation` */

CREATE TABLE `general_infomation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(100) DEFAULT NULL,
  `student_name` varchar(100) DEFAULT NULL,
  `seeking_grade` varchar(50) DEFAULT NULL,
  `current_age_month` varchar(50) DEFAULT NULL,
  `current_age_year` varchar(50) DEFAULT NULL,
  `contact_Number` varchar(50) DEFAULT NULL,
  `email_id` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `Enquiry` varchar(10000) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `enquiryNo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `general_infomation` */

insert  into `general_infomation`(`id`,`contact_name`,`student_name`,`seeking_grade`,`current_age_month`,`current_age_year`,`contact_Number`,`email_id`,`city`,`country`,`Enquiry`,`date`,`enquiryNo`) values (1,'santosh.sahu','aryan','Junior KG','5','3','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(2,'santosh.sahu','aryan','Senior KG','2','4','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-03-31','EQ5601'),(3,'santosh.sahu','aryan','Senior KG','4','6','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-03-31','EQ5601'),(4,'santosh.sahu','aryan','Grade 1','6','7','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-03-31','EQ5601'),(5,'santosh.sahu','aryan','Grade 1','6','7','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-03-31','EQ5601'),(6,'Ramesh.Sharma','Monoj','Grade 3','5','7','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(7,'santosh','Ramesh','Grade 3','5','8','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(8,'santosh','Surya','Grade 1','4','9','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(9,'santosh','Raja','Grade 3','5','8','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(10,'santosh','Jagdish','Grade 2','5','6','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(11,'santosh','David','Grade 3','6','9','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(12,'santosh','Jhonson','Grade 3','6','8','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(13,'santosh','Swathi','Grade 2','7','8','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601'),(15,'santosh','sahu','Grade 1','2','3','9008365317','santosh.citech@gmail.com','bangalore','india','Hi i am santosh here seeking the Opportunity for the Grade 3','2015-02-26','EQ5601');

/*Table structure for table `homework_upload_table` */

CREATE TABLE `homework_upload_table` (
  `homework_upload_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `staff_info_id` bigint(20) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `parent_info_id` bigint(20) DEFAULT NULL,
  `subject` varchar(40) DEFAULT NULL,
  `homework_file` tinyblob,
  `homework_upload_date` datetime DEFAULT NULL,
  PRIMARY KEY (`homework_upload_table_id`),
  KEY `FK977B537F494DC146` (`parent_info_id`),
  KEY `FK977B537F3210A52B` (`student_id`),
  KEY `FK977B537FE0194234` (`staff_info_id`),
  KEY `FK977B537FD8EC3401` (`parent_info_id`),
  KEY `FK977B537F964089D0` (`student_id`),
  KEY `FK977B537F47D40C19` (`staff_info_id`),
  CONSTRAINT `FK977B537F3210A52B` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK977B537F47D40C19` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `FK977B537F494DC146` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `FK977B537F964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK977B537FD8EC3401` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `FK977B537FE0194234` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `homework_upload_table` */

/*Table structure for table `imagegallary` */

CREATE TABLE `imagegallary` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `UploadImage` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `imagegallary` */

/*Table structure for table `parent_approve_activity` */

CREATE TABLE `parent_approve_activity` (
  `parent_approve_activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_id` bigint(20) DEFAULT NULL,
  `activity_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`parent_approve_activity_id`),
  KEY `FK98700AB681ED5D3F` (`activity_id`),
  KEY `FK98700AB63210A52B` (`student_id`),
  KEY `FK98700AB6AE7AE1A4` (`activity_id`),
  KEY `FK98700AB6964089D0` (`student_id`),
  CONSTRAINT `FK98700AB6964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK98700AB6AE7AE1A4` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`),
  CONSTRAINT `parent_approve_activity_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `parent_approve_activity_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `parent_approve_activity` */

insert  into `parent_approve_activity`(`parent_approve_activity_id`,`student_id`,`activity_id`) values (1,2,5),(2,3,5),(3,2,8),(4,3,8),(5,2,10),(6,3,10),(7,2,6),(8,3,6),(9,2,7),(10,3,7),(11,2,12),(12,3,12),(13,2,25),(14,3,25),(15,2,19),(16,5,6),(17,5,12),(18,5,19),(19,5,20),(20,5,11),(21,2,11);

/*Table structure for table `parent_inbox_table` */

CREATE TABLE `parent_inbox_table` (
  `parent_inbox_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbox_message` varchar(500) DEFAULT NULL,
  `parent_info_id` bigint(20) DEFAULT NULL,
  `inbox_subject` varchar(100) DEFAULT NULL,
  `sender_name` varchar(100) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `sender_designation` varchar(20) DEFAULT NULL,
  `sender_standard` varchar(20) DEFAULT NULL,
  `sender_section` varchar(10) DEFAULT NULL,
  `sender_subject` varchar(20) DEFAULT NULL,
  `sender_id` int(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`parent_inbox_id`),
  KEY `parent_info_id` (`parent_info_id`),
  KEY `FKBD16FCC0494DC146` (`parent_info_id`),
  KEY `FKBD16FCC0D8EC3401` (`parent_info_id`),
  CONSTRAINT `FKBD16FCC0D8EC3401` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `parent_inbox_table_ibfk_1` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `parent_inbox_table` */

insert  into `parent_inbox_table`(`parent_inbox_id`,`inbox_message`,`parent_info_id`,`inbox_subject`,`sender_name`,`date`,`sender_designation`,`sender_standard`,`sender_section`,`sender_subject`,`sender_id`,`status`) values (1,'I\'m trying to figure out how to log out a user with an f:ajax call using JSF and a backing managed bean. The problem I\'m having is that I can\'t quite figure out why the call order for the Ajax listener and the rerender of the login form.\r\nBelow is the very simplified code. The basic idea of the code is this\r\n',3,'Announcement','Srinivas','0000-00-00 00:00:00','Principal',NULL,NULL,'Principal',0,1),(6,'The problem with the code is that when clicking logout the form reloads but it is still in the logged in state even though the uid has been set to null. I\'ve checked this with the debugger. So how do I make the render be executed AFTER the ajax listener?\r\n\r\n',3,'FeedBack From Staff','Srinivas - Principal','Nov-25-2013 18:32:44','staff',NULL,NULL,'Principal',1,1),(7,'The JSTL tags are executed during view build time, not during view render time. At the moment JSF is reusing the view state for re-render, the JSTL tags won\'t be re-executed, simply because they are not there anymore.',3,'Feedback from the Staff that Your child is ot weoll','czxczxc',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'Now that that\'s done, we can style the label, but more specifically the span inside of the label. I do it this way in order to give myself more control over the exact position of the check box. Without it you would probably be using a background image in the label directly, and positioning it may become difficult.',3,'FeedBack From Staff','Srinivas - Principal','Mar-24-2014 15:36:15','staff',NULL,NULL,'Principal',1,1),(16,'Alright, let me explain this quickly. First off, notice the background. I have a small sprite sheet for these, so all I have to do is set the background position on this span. The span itself is the exact width and height of each \"sprite\" in the sheet, making it easy to define each state.',3,'FeedBack From Staff','Srinivas - Principal','Mar-24-2014 16:09:30','staff',NULL,NULL,'Principal',1,1),(17,'Now that that\'s done, we can style the label, but more specifically the span inside of the label. I do it this way in order to give myself more control over the exact position of the check box. Without it you would probably be using a background image in the label directly, and positioning it may become difficult.',3,'FeedBack From Staff','Srinivas - Principal','Mar-24-2014 16:09:47','staff',NULL,NULL,'Principal',1,1),(18,'Alright, let me explain this quickly. First off, notice the background. I have a small sprite sheet for these, so all I have to do is set the background position on this span. The span itself is the exact width and height of each \"sprite\" in the sheet, making it easy to define each state.',3,'FeedBack From Staff','Srinivas - Principal','Mar-24-2014 16:09:56','staff',NULL,NULL,'Principal',1,1),(19,'Alright, let me explain this quickly. First off, notice the background. I have a small sprite sheet for these, so all I have to do is set the background position on this span. The span itself is the exact width and height of each \"sprite\" in the sheet, making it easy to define each state.',1,'feedback from staff','Srinivas - Principal','Mar-24-2014 16:10:07','staff',NULL,NULL,'Principal',1,0),(20,'Alright, let me explain this quickly. First off, notice the background. I have a small sprite sheet for these, so all I have to do is set the background position on this span. The span itself is the exact width and height of each \"sprite\" in the sheet, making it easy to define each state.',1,'feedback from staff','Srinivas - Principal','Mar-24-2014 16:10:14','staff',NULL,NULL,'Principal',1,0),(24,'Now that that\'s done, we can style the label, but more specifically the span inside of the label. I do it this way in order to give myself more control over the exact position of the check box. Without it you would probably be using a background image in the label directly, and positioning it may become difficult.',1,'feedback from staff','Srinivas - Principal','Mar-24-2014 16:11:35','staff',NULL,NULL,'Principal',1,0),(25,'Alright, let me explain this quickly. First off, notice the background. I have a small sprite sheet for these, so all I have to do is set the background position on this span. The span itself is the exact width and height of each \"sprite\" in the sheet, making it easy to define each state.',1,'feedback from staff','Srinivas - Principal','Mar-24-2014 16:11:42','staff',NULL,NULL,'Principal',1,0),(26,'Now that that\'s done, we can style the label, but more specifically the span inside of the label. I do it this way in order to give myself more control over the exact position of the check box. Without it you would probably be using a background image in the label directly, and positioning it may become difficult.',1,'feedback from staff','Srinivas - Principal','Mar-24-2014 16:11:57','staff',NULL,NULL,'Principal',1,0),(27,'Alright, let me explain this quickly. First off, notice the background. I have a small sprite sheet for these, so all I have to do is set the background position on this span. The span itself is the exact width and height of each \"sprite\" in the sheet, making it easy to define each state.',3,'FeedBack From Staff','Srinivas - Principal','Mar-24-2014 16:12:10','staff',NULL,NULL,'Principal',1,1),(28,'Now that that\'s done, we can style the label, but more specifically the span inside of the label. I do it this way in order to give myself more control over the exact position of the check box. Without it you would probably be using a background image in the label directly, and positioning it may become difficult.',1,'feedback from staff','Srinivas - Principal','Mar-24-2014 16:12:23','staff',NULL,NULL,'Principal',1,0),(29,'Now that that\'s done, we can style the label, but more specifically the span inside of the label. I do it this way in order to give myself more control over the exact position of the check box. Without it you would probably be using a background image in the label directly, and positioning it may become difficult.',1,'feedback from staff','MOHAN - Teacher','Mar-26-2014 00:21:55','staff',NULL,NULL,'Hindi',10,0),(30,'hi this is message from prin',1,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:55:51','staff',NULL,NULL,'Principal',1,0);

/*Table structure for table `parent_info_table` */

CREATE TABLE `parent_info_table` (
  `parent_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_name` varchar(50) DEFAULT NULL,
  `parent_sir_name` varchar(20) DEFAULT NULL,
  `parent_address` varchar(500) DEFAULT NULL,
  `parent_phone_no` varchar(15) DEFAULT NULL,
  `parent_user_name` varchar(20) DEFAULT NULL,
  `parent_password` varchar(20) DEFAULT NULL,
  `parent_gender` varchar(15) DEFAULT NULL,
  `parent_designation` varchar(40) DEFAULT NULL,
  `parent_company_name` varchar(40) DEFAULT NULL,
  `parent_relation_ship` varchar(30) DEFAULT NULL,
  `parent_email_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`parent_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `parent_info_table` */

insert  into `parent_info_table`(`parent_info_id`,`parent_name`,`parent_sir_name`,`parent_address`,`parent_phone_no`,`parent_user_name`,`parent_password`,`parent_gender`,`parent_designation`,`parent_company_name`,`parent_relation_ship`,`parent_email_id`) values (1,'parentA','Agarwal','bangalore','(888) 456-9727','parent','parent','Male','Software Engineer','IBM','Father','mohan.agarwal@gmail.com'),(3,'suresh','upreti','Uttranchal Almora','9999999999','parent2','parent2','Male','father','goverment ','Father','mohan@gmail.com'),(4,'Ramesh','SHARMA','Bangalore','9008365317','Ramesh_Netberry','parent123456','Male','Software Engineer','Netberry','Father','parent@gmail.com'),(5,'santosh','sahu','Bangalore','9008365317','santosh_netberry','santosh123456','Male','Software Engineer','Netberry','Father','santosh.citech@gmail.com'),(6,'Harish','Sharma','Kalyani Magnum,\n165/2, DS Palya, JP Nagar 7th Phase,Bannerghatta Road,\n','9008365317','Harish.TechSystem','Harish123','Male','Application Support','CTS','Father','Harish.CTS@gmail.com');

/*Table structure for table `parent_photos_table` */

CREATE TABLE `parent_photos_table` (
  `parent_photo_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` mediumtext,
  `parent_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`parent_photo_id`),
  KEY `parent_id` (`parent_id`),
  KEY `FK43A04805419B665F` (`parent_id`),
  KEY `FK43A04805D139D91A` (`parent_id`),
  CONSTRAINT `FK43A04805D139D91A` FOREIGN KEY (`parent_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `parent_photos_table_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `parent_info_table` (`parent_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `parent_photos_table` */

/*Table structure for table `parent_sent_items_table` */

CREATE TABLE `parent_sent_items_table` (
  `parent_sent_items_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sent_items_message` varchar(500) DEFAULT NULL,
  `parent_info_id` bigint(20) DEFAULT NULL,
  `sent_items_subject` varchar(100) DEFAULT NULL,
  `sender_name` varchar(20) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `sender_designation` varchar(20) DEFAULT NULL,
  `sender_standard` varchar(20) DEFAULT NULL,
  `sender_section` varchar(100) DEFAULT NULL,
  `sender_subject` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`parent_sent_items_id`),
  KEY `staff_info_id` (`parent_info_id`),
  KEY `FK7A6CC35D494DC146` (`parent_info_id`),
  KEY `FK7A6CC35DD8EC3401` (`parent_info_id`),
  CONSTRAINT `FK7A6CC35DD8EC3401` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `parent_sent_items_table_ibfk_1` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `parent_sent_items_table` */

insert  into `parent_sent_items_table`(`parent_sent_items_id`,`sent_items_message`,`parent_info_id`,`sent_items_subject`,`sender_name`,`date`,`sender_designation`,`sender_standard`,`sender_section`,`sender_subject`,`status`) values (2,'sir/madam..\nthese is to inform that .my son having a problem in understanding mathematics .he said that teacher is having problem in explaining the solution..so please kindly look over it..\nthank u',3,'Feedback sent to Staff','Class Teacher','Nov-18-2013 14:23:20','Class Teacher','5','A','Maths',1),(14,'dadasda',1,'feedback sent to staff','Srinivas','Mar-19-2014 11:24:33','Principal','','','Principal',0),(15,'sdaadasd',1,'feedback sent to staff','Class Teacher','Mar-19-2014 11:24:37','Class Teacher','5','A','Maths',0),(16,'rrrrrrrrr',1,'feedback sent to staff','Subash','Mar-19-2014 11:24:45','Vice Principal','',NULL,'Vice',0),(17,'03-22-2014,03-27-2014,fggdfg',1,'leave application','Srinivas','03-21-2014 15:04:59','Principal','5','A','Principal',0),(18,'dasdasd',1,'feedback sent to staff','Subash','Mar-24-2014 14:12:48','Vice Principal','',NULL,'Vice',0),(20,'dasdasd',1,'feedback sent to staff','Srinivas','Mar-24-2014 14:12:56','Principal','','','Principal',0),(21,'dasdasd',1,'feedback sent to staff','Subash','Mar-24-2014 15:40:29','Vice Principal','',NULL,'Vice',0),(23,'dasdasd',1,'feedback sent to staff','Class Teacher','Mar-24-2014 15:40:36','Class Teacher','5','A','Maths',0),(24,'dasdasd',1,'feedback sent to staff','Srinivas','Mar-24-2014 15:40:39','Principal','','','Principal',0),(25,'czxczxc',1,'feedback sent to staff','Class Teacher','Mar-24-2014 15:46:18','Class Teacher','5','A','Maths',0),(26,'czczxcz',1,'feedback sent to staff','Subash','Mar-24-2014 15:46:25','Vice Principal','',NULL,'Vice',0);

/*Table structure for table `parent_timetable` */

CREATE TABLE `parent_timetable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_info_id` bigint(20) DEFAULT NULL,
  `Parent Name` varchar(20) DEFAULT NULL,
  `Inbox Messages` varchar(20) DEFAULT NULL,
  `OutBox Messages` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKDAEF57CC494DC146` (`parent_info_id`),
  CONSTRAINT `FKDAEF57CC494DC146` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `parent_timetable` */

/*Table structure for table `point_bucket_answer_table` */

CREATE TABLE `point_bucket_answer_table` (
  `answer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question_id` bigint(20) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `class_id` bigint(20) DEFAULT NULL,
  `answer` varchar(10) DEFAULT NULL,
  `points` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `FK1AA41F1355D3337F` (`class_id`),
  KEY `FK1AA41F133210A52B` (`student_id`),
  KEY `FK1AA41F1378EB06E9` (`question_id`),
  KEY `FK1AA41F138260B7E4` (`class_id`),
  KEY `FK1AA41F13964089D0` (`student_id`),
  KEY `FK1AA41F13C112768E` (`question_id`),
  CONSTRAINT `FK1AA41F133210A52B` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK1AA41F1355D3337F` FOREIGN KEY (`class_id`) REFERENCES `class_table` (`class_table_id`),
  CONSTRAINT `FK1AA41F1378EB06E9` FOREIGN KEY (`question_id`) REFERENCES `point_bucket_question_table` (`question_id`),
  CONSTRAINT `FK1AA41F138260B7E4` FOREIGN KEY (`class_id`) REFERENCES `class_table` (`class_table_id`),
  CONSTRAINT `FK1AA41F13964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK1AA41F13C112768E` FOREIGN KEY (`question_id`) REFERENCES `point_bucket_question_table` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `point_bucket_answer_table` */

insert  into `point_bucket_answer_table`(`answer_id`,`question_id`,`student_id`,`class_id`,`answer`,`points`) values (1,1,2,13,'A',20),(2,1,2,14,'B',20),(3,2,2,70,'A',20),(4,3,5,71,'C',30),(5,4,8,72,'A',0),(6,5,7,73,'B',0);

/*Table structure for table `point_bucket_question_table` */

CREATE TABLE `point_bucket_question_table` (
  `question_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `staff_id` bigint(20) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `question` varchar(250) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `option_a` varchar(30) DEFAULT NULL,
  `option_b` varchar(30) DEFAULT NULL,
  `option_c` varchar(30) DEFAULT NULL,
  `option_d` varchar(30) DEFAULT NULL,
  `answer` char(1) DEFAULT NULL,
  PRIMARY KEY (`question_id`),
  KEY `FK3E62B3BBB8D85521` (`staff_id`),
  KEY `FK3E62B3BB419B665F` (`parent_id`),
  KEY `FK3E62B3BB20931F06` (`staff_id`),
  KEY `FK3E62B3BBD139D91A` (`parent_id`),
  CONSTRAINT `FK3E62B3BB20931F06` FOREIGN KEY (`staff_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `FK3E62B3BB419B665F` FOREIGN KEY (`parent_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `FK3E62B3BBB8D85521` FOREIGN KEY (`staff_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `FK3E62B3BBD139D91A` FOREIGN KEY (`parent_id`) REFERENCES `parent_info_table` (`parent_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `point_bucket_question_table` */

insert  into `point_bucket_question_table`(`question_id`,`staff_id`,`parent_id`,`question`,`date`,`option_a`,`option_b`,`option_c`,`option_d`,`answer`) values (1,1,1,'Which language is your mother tounge \r\n','2015-02-19','Hindi','bengali','marathi','sanskrit','A'),(2,1,1,'What are the principle concepts of OOPS?','2015-03-20','Abstraction','Polymorphism','Inheritance','Encapsulation','B'),(3,1,1,'What is Linux','2015-03-20','OS','Linux','unix ','Linux','b'),(4,1,1,'What are differences between struts and JSF','2015-03-20','FrameWork','HomeWork','NeedWork','None ofthem ','A'),(5,1,1,'What is Managed Bean','2015-03-20','Bean','soyabean','none of them','application','A'),(6,1,1,'What is the range of data type short in Java?','2015-03-20','-128 to 127',' -32768 to 32767',' -2147483648 to 2147483647','None of the mentioned','A'),(7,1,1,' An expression involving byte, int, and literal numbers is promoted to which of these','2015-03-20','int','long','byte','float','b'),(8,1,1,'Which of these literals can be contained in a data type float variable','2015-03-20','1.7e-308','3.4e-038','1.7e+308',' 3.4e-050','A'),(9,1,1,'Which data type value is returned by all transcendental math functions','2015-03-20','int','float','long ','double','B'),(10,1,1,'Which of these is returned by greater than, <, and equal to, ==, operator?','2015-03-20','Integers','Floating - point numbers','Boolean','None of the mentioned','A'),(11,1,1,'Which of the following operators can operate on a boolean variable?','2015-03-20','boolean','less than','greater than ','none of these','B');

/*Table structure for table `route_assign_table` */

CREATE TABLE `route_assign_table` (
  `route_assign_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vehicle_id` bigint(20) DEFAULT NULL,
  `route_id` bigint(20) DEFAULT NULL,
  `driver_id` bigint(20) DEFAULT NULL,
  `driver_info_table_id` bigint(20) DEFAULT NULL,
  `route_number` varchar(40) DEFAULT NULL,
  `vehicle_number` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`route_assign_table_id`),
  KEY `FK7A22E6B47494FEDD` (`route_id`),
  KEY `FK7A22E6B439C9BC63` (`driver_id`),
  KEY `FK7A22E6B4251998C9` (`vehicle_id`),
  KEY `FK7A22E6B46F12E397` (`driver_info_table_id`),
  KEY `FK7A22E6B4FEB15652` (`driver_info_table_id`),
  CONSTRAINT `FK7A22E6B4251998C9` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle_info_table` (`vehicle_info_table_id`),
  CONSTRAINT `FK7A22E6B439C9BC63` FOREIGN KEY (`driver_id`) REFERENCES `driver_info_table` (`driver_info_table_id`),
  CONSTRAINT `FK7A22E6B46F12E397` FOREIGN KEY (`driver_info_table_id`) REFERENCES `driver_info_table` (`driver_info_table_id`),
  CONSTRAINT `FK7A22E6B47494FEDD` FOREIGN KEY (`route_id`) REFERENCES `route_table` (`route_table_id`),
  CONSTRAINT `FK7A22E6B4FEB15652` FOREIGN KEY (`driver_info_table_id`) REFERENCES `driver_info_table` (`driver_info_table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `route_assign_table` */

/*Table structure for table `route_table` */

CREATE TABLE `route_table` (
  `route_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `starting_point` varchar(40) DEFAULT NULL,
  `via` varchar(100) DEFAULT NULL,
  `route_number` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`route_table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `route_table` */

/*Table structure for table `staff_attendance_table` */

CREATE TABLE `staff_attendance_table` (
  `staff_attendance_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `month` varchar(15) DEFAULT NULL,
  `d1` varchar(10) DEFAULT NULL,
  `d2` varchar(10) DEFAULT NULL,
  `d3` varchar(10) DEFAULT NULL,
  `d4` varchar(10) DEFAULT NULL,
  `d5` varchar(10) DEFAULT NULL,
  `d6` varchar(10) DEFAULT NULL,
  `d7` varchar(10) DEFAULT NULL,
  `d8` varchar(10) DEFAULT NULL,
  `d9` varchar(10) DEFAULT NULL,
  `d10` varchar(10) DEFAULT NULL,
  `d11` varchar(10) DEFAULT NULL,
  `d12` varchar(10) DEFAULT NULL,
  `d13` varchar(10) DEFAULT NULL,
  `d14` varchar(10) DEFAULT NULL,
  `d15` varchar(10) DEFAULT NULL,
  `d16` varchar(10) DEFAULT NULL,
  `d17` varchar(10) DEFAULT NULL,
  `d18` varchar(10) DEFAULT NULL,
  `d19` varchar(10) DEFAULT NULL,
  `d20` varchar(10) DEFAULT NULL,
  `d21` varchar(10) DEFAULT NULL,
  `d22` varchar(10) DEFAULT NULL,
  `d23` varchar(10) DEFAULT NULL,
  `d24` varchar(10) DEFAULT NULL,
  `d25` varchar(10) DEFAULT NULL,
  `d26` varchar(10) DEFAULT NULL,
  `d27` varchar(10) DEFAULT NULL,
  `d28` varchar(10) DEFAULT NULL,
  `d29` varchar(10) DEFAULT NULL,
  `d30` varchar(10) DEFAULT NULL,
  `d31` varchar(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `staff_info_id` bigint(20) DEFAULT NULL,
  `staff_name` varchar(50) DEFAULT NULL,
  `staff_standard` varchar(20) DEFAULT NULL,
  `staff_section` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`staff_attendance_id`),
  KEY `staff_info_id` (`staff_info_id`),
  KEY `FK2D1FAA77E0194234` (`staff_info_id`),
  KEY `FK2D1FAA7747D40C19` (`staff_info_id`),
  CONSTRAINT `FK2D1FAA7747D40C19` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `staff_attendance_table_ibfk_2` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `staff_attendance_table` */

insert  into `staff_attendance_table`(`staff_attendance_id`,`month`,`d1`,`d2`,`d3`,`d4`,`d5`,`d6`,`d7`,`d8`,`d9`,`d10`,`d11`,`d12`,`d13`,`d14`,`d15`,`d16`,`d17`,`d18`,`d19`,`d20`,`d21`,`d22`,`d23`,`d24`,`d25`,`d26`,`d27`,`d28`,`d29`,`d30`,`d31`,`year`,`staff_info_id`,`staff_name`,`staff_standard`,`staff_section`) values (1,'May','A','S','H','S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',1,NULL,NULL,NULL),(2,'May','P','P','H','A','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',4,NULL,NULL,NULL),(4,'May','P','P','H','P','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',6,NULL,NULL,NULL),(5,'May','P','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',7,NULL,NULL,NULL),(6,'May','P','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',8,NULL,NULL,NULL),(7,'May','P','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',9,NULL,NULL,NULL),(8,'May','P','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',10,NULL,NULL,NULL);

/*Table structure for table `staff_book_issue_details` */

CREATE TABLE `staff_book_issue_details` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `staff_role` varchar(40) DEFAULT NULL,
  `staff_name` varchar(40) DEFAULT NULL,
  `no_of_days` bigint(40) DEFAULT NULL,
  `book_id` varchar(40) DEFAULT NULL,
  `category_name` varchar(40) DEFAULT NULL,
  `title` varchar(40) DEFAULT NULL,
  `author_name` varchar(40) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;

/*Data for the table `staff_book_issue_details` */

insert  into `staff_book_issue_details`(`slno`,`staff_role`,`staff_name`,`no_of_days`,`book_id`,`category_name`,`title`,`author_name`,`description`,`issued_date`) values (90,'Lecture','Srinivas',7,'b222','Social','C','David Sne','sdfg','2015-04-15'),(91,'Lecture','Srinivas',7,'b123','Sceince','Java','santosh kiran','sdfasdfg','2015-04-15'),(92,'Lecture','Srinivas',7,'b123','Sceince','Java','santosh kiran','sdfasdfg','2015-04-15'),(93,'Class Teacher','ABHAY ',7,'b123','Sceince','Java','santosh kiran','','2015-05-27'),(94,'Class Teacher','ABHAY ',7,'b222','Social','C','David Sne','','2015-07-02'),(95,'Class Teacher','ABHAY ',7,'b222','Social','C','David Sne','','2015-07-02'),(96,'','',7,'B115','Business','JAVA','Nancy Buckingham','ccccccc','2015-09-15');

/*Table structure for table `staff_inbox_table` */

CREATE TABLE `staff_inbox_table` (
  `staff_inbox_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inbox_message` varchar(500) DEFAULT NULL,
  `staff_info_id` bigint(20) DEFAULT NULL,
  `inbox_subject` varchar(100) DEFAULT NULL,
  `sender_name` varchar(100) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `sender_designation` varchar(20) DEFAULT NULL,
  `sender_standard` varchar(20) DEFAULT NULL,
  `sender_section` varchar(10) DEFAULT NULL,
  `sender_status` varchar(20) DEFAULT NULL,
  `sender_subject` varchar(30) DEFAULT NULL,
  `sender_id` int(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`staff_inbox_id`),
  KEY `staff_info_id` (`staff_info_id`),
  KEY `FKAAD315F6E0194234` (`staff_info_id`),
  KEY `FKAAD315F647D40C19` (`staff_info_id`),
  CONSTRAINT `FKAAD315F647D40C19` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `staff_inbox_table_ibfk_1` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `staff_inbox_table` */

insert  into `staff_inbox_table`(`staff_inbox_id`,`inbox_message`,`staff_info_id`,`inbox_subject`,`sender_name`,`date`,`sender_designation`,`sender_standard`,`sender_section`,`sender_status`,`sender_subject`,`sender_id`,`status`) values (1,'Hi Rohit How are You ',6,'feedback from staff','Srinivas - Principal','Apr-10-2015 13:42:17','Principal','Principal','Principal','Staff',NULL,1,0),(2,'sadasdasdasdasdas',6,'feedback from staff','Srinivas - Principal','Apr-10-2015 14:00:28','Principal','Principal','Principal','Staff',NULL,1,0),(3,'ddddddddddddddddd',6,'feedback from staff','Srinivas - Principal','Apr-10-2015 14:00:32','Principal','Principal','Principal','Staff',NULL,1,0),(4,'ccccccccccccccccccccc',6,'feedback from staff','Srinivas - Principal','Apr-10-2015 14:00:37','Principal','Principal','Principal','Staff',NULL,1,0),(5,'<br>',1,'feedback from student','Mohan','Sep-08-2015 13:06:13','Student','Class I','A','Student',NULL,2,0),(6,'You Mark is Too Bad',4,'FeedBack From Staff','Srinivas - Principal','Sep-08-2015 13:10:48','Principal','Principal','Principal','Staff',NULL,1,1),(7,'hi sri<br>',1,'feedback from student','Mohan','Sep-08-2015 13:54:35','Student','Class I','A','Student',NULL,2,0);

/*Table structure for table `staff_info_table` */

CREATE TABLE `staff_info_table` (
  `staff_info_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `staff_name` varchar(50) DEFAULT NULL,
  `staff_address` varchar(300) DEFAULT NULL,
  `staff_phone_no` varchar(15) DEFAULT NULL,
  `staff_gender` varchar(10) DEFAULT NULL,
  `staff_user_name` varchar(20) DEFAULT NULL,
  `staff_password` varchar(20) DEFAULT NULL,
  `staff_doj` date DEFAULT NULL,
  `staff_email_id` varchar(100) DEFAULT NULL,
  `staff_roles` varchar(300) DEFAULT NULL,
  `staff_subject` varchar(20) DEFAULT NULL,
  `staff_sir_name` varchar(20) DEFAULT NULL,
  `staff_classteacher_standard` varchar(50) DEFAULT NULL,
  `staff_classteacher_section` varchar(10) DEFAULT NULL,
  `staff_standard1` varchar(50) DEFAULT NULL,
  `staff_section1` varchar(10) DEFAULT NULL,
  `staff_standard2` varchar(50) DEFAULT NULL,
  `staff_section2` varchar(10) DEFAULT NULL,
  `staff_standard3` varchar(50) DEFAULT NULL,
  `staff_section3` varchar(10) DEFAULT NULL,
  `staff_standard4` varchar(50) DEFAULT NULL,
  `staff_section4` varchar(10) DEFAULT NULL,
  `staff_standard5` varchar(50) DEFAULT NULL,
  `staff_section5` varchar(10) DEFAULT NULL,
  `staff_standard6` varchar(50) DEFAULT NULL,
  `staff_section6` varchar(10) DEFAULT NULL,
  `staff_standard7` varchar(50) DEFAULT NULL,
  `staff_section7` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`staff_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `staff_info_table` */

insert  into `staff_info_table`(`staff_info_id`,`staff_name`,`staff_address`,`staff_phone_no`,`staff_gender`,`staff_user_name`,`staff_password`,`staff_doj`,`staff_email_id`,`staff_roles`,`staff_subject`,`staff_sir_name`,`staff_classteacher_standard`,`staff_classteacher_section`,`staff_standard1`,`staff_section1`,`staff_standard2`,`staff_section2`,`staff_standard3`,`staff_section3`,`staff_standard4`,`staff_section4`,`staff_standard5`,`staff_section5`,`staff_standard6`,`staff_section6`,`staff_standard7`,`staff_section7`) values (1,'Srinivas','NGEF Premises, Old Madras Road,','8892763770','Male','principal','principal','2013-08-08','mohan@gmail.com','Principal','Principal','Rao','Class V','B',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Harish','North Anjaneya Temple Street,','9008365317','Male','Harish.Netberry','12356','2015-03-27','santosh.citech@gmail.com','Maintenance ','History','sahu','Class II','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'ABHAY ',' 3804, Magzine Street, Churi Walan, Delhi India','9008365317','Male','ABHAY.Netberry','123456','2015-04-03','ABHAY@gmail.com','Class Teacher','Maths','Sharma','Class I','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Rohit','Rajasthnai Udyog Nagar, G.t. Karnal Road Delhi India','9008365317','Male','rohit.netberry','123456','2015-04-10','rohit.sharma@gmail.com','librarian',NULL,'Agarwal','CLASS V',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'ARJUN','Bangalore HSR LAYOUT','9008365317','Male','arjun','arjun123456','2015-04-10','arjun@gmail.com','fee collector',NULL,'SONE','BCA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Harish',' 	Ist Floor, ESID Bldg. IInd Cross,\r\nOTC Road, B’lore-53','9008365317','Male','Harish.Sekhar','123456','2015-04-10','Harish@gmail.com','Class Teacher',NULL,'sharma',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Shreya',' 	ESID Bldg, Ist Cross, Magadi Road,\r\nBangalore-23','9008365317','Female','Shreya@123456','123456','2015-04-10','shreya@yahoo.com','Class Teacher',NULL,'Basu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'Radhika','IstFloor,ESI Disp.Bldg, peenya,\r\nBangalore – 560 058\r\nIIIrd phase, Peenya, Bangalore-58 ','9008365317','Female','Radhika','Radhika123456','2015-04-10','radhika@gmail.com','Class Teacher',NULL,'sahu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `staff_photos_table` */

CREATE TABLE `staff_photos_table` (
  `staff_photo_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` mediumtext,
  `staff_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`staff_photo_id`),
  KEY `FKD67558FB8D85521` (`staff_id`),
  KEY `FKD67558F20931F06` (`staff_id`),
  CONSTRAINT `FKD67558F20931F06` FOREIGN KEY (`staff_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `staff_photos_table_ibfk_2` FOREIGN KEY (`staff_id`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `staff_photos_table` */

insert  into `staff_photos_table`(`staff_photo_id`,`image`,`staff_id`) values (1,'tier-architecture-will-talk-about-implementing-42313.png',1),(2,'3tier (2).jpg',1),(3,'690dfe7abd49ec4acee97167329c7335.png',1);

/*Table structure for table `staff_sent_items_table` */

CREATE TABLE `staff_sent_items_table` (
  `staff_sent_items_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sent_items_message` varchar(5000) DEFAULT NULL,
  `staff_info_id` bigint(20) DEFAULT NULL,
  `sent_items_subject` varchar(100) DEFAULT NULL,
  `sender_name` varchar(20) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `sender_designation` varchar(20) DEFAULT NULL,
  `sender_standard` varchar(20) DEFAULT NULL,
  `sender_section` varchar(10) DEFAULT NULL,
  `sender_subject` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`staff_sent_items_id`),
  KEY `staff_info_id` (`staff_info_id`),
  KEY `FKC77DFBE7E0194234` (`staff_info_id`),
  KEY `FKC77DFBE747D40C19` (`staff_info_id`),
  CONSTRAINT `FKC77DFBE747D40C19` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `staff_sent_items_table_ibfk_1` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `staff_sent_items_table` */

insert  into `staff_sent_items_table`(`staff_sent_items_id`,`sent_items_message`,`staff_info_id`,`sent_items_subject`,`sender_name`,`date`,`sender_designation`,`sender_standard`,`sender_section`,`sender_subject`,`status`) values (1,'Hi Rohit How are You ',1,'feedback to staff','Rohit','Apr-10-2015 13:42:17','librarian',NULL,NULL,NULL,0),(2,'sadasdasdasdasdas',1,'FeedBack To Staff','Rohit','Apr-10-2015 14:00:28','librarian',NULL,NULL,NULL,1),(3,'ddddddddddddddddd',1,'feedback to staff','Rohit','Apr-10-2015 14:00:32','librarian',NULL,NULL,NULL,0),(4,'ccccccccccccccccccccc',1,'feedback to staff','Rohit','Apr-10-2015 14:00:37','librarian',NULL,NULL,NULL,0),(5,'You Mark is Too Bad',1,'FeedBack To Staff','Harish','Sep-08-2015 13:10:48','Maintenance ','Class II','A','History',1),(6,'You Marks is Too Low in Maths Please Try to Improve It',1,'FeedBack To Student','Mohan','Sep-08-2015 13:12:36','student','Class I','A','Principal',1),(7,'hi this is message from prin',1,'feedback to parent','Mohan\'s  Parent','Sep-08-2015 13:55:51','parent','Class I','A','Principal',0);

/*Table structure for table `student_admission` */

CREATE TABLE `student_admission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `child_name` varchar(100) DEFAULT NULL,
  `child_last_name` varchar(100) DEFAULT NULL,
  `seeking_grade` varchar(100) DEFAULT NULL,
  `academic_year` varchar(100) DEFAULT NULL,
  `date_of_birth` varchar(100) DEFAULT NULL,
  `palec_of_birth` varchar(100) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  `mother_tongue` varchar(100) DEFAULT NULL,
  `religion` varchar(100) DEFAULT NULL,
  `nationality` varchar(100) DEFAULT NULL,
  `ci_ResidentalAddress` varchar(500) DEFAULT NULL,
  `ci_city` varchar(100) DEFAULT NULL,
  `ci_postalcode` varchar(100) DEFAULT NULL,
  `ci_landmark` varchar(100) DEFAULT NULL,
  `ci_state` varchar(100) DEFAULT NULL,
  `ci_telephoneno` varchar(100) DEFAULT NULL,
  `ci_country` varchar(100) DEFAULT NULL,
  `dlsa_Name_of_school` varchar(100) DEFAULT NULL,
  `dlsa_Curriculum_Board` varchar(100) DEFAULT NULL,
  `dlsa_grade_class` varchar(100) DEFAULT NULL,
  `dlsa_state` varchar(100) DEFAULT NULL,
  `dlsa_city` varchar(100) DEFAULT NULL,
  `dlsa_percentageofmarks` varchar(100) DEFAULT NULL,
  `dlsa_postalcode` varchar(100) DEFAULT NULL,
  `dlsa_address` varchar(100) DEFAULT NULL,
  `dlsa_postal` varchar(100) DEFAULT NULL,
  `dlsa_country` varchar(100) DEFAULT NULL,
  `dlsa_telephone` varchar(100) DEFAULT NULL,
  `dlsa_reason_for_change` varchar(100) DEFAULT NULL,
  `dlsa_grade_repeat` varchar(100) DEFAULT NULL,
  `guardian1_Relationship_with_child` varchar(100) DEFAULT NULL,
  `guardian1_Name` varchar(100) DEFAULT NULL,
  `guardian1_last` varchar(100) DEFAULT NULL,
  `guardian1_DateofBirth` varchar(100) DEFAULT NULL,
  `guardian1_Qualification` varchar(100) DEFAULT NULL,
  `guardian1_Occupation` varchar(100) DEFAULT NULL,
  `guardian1_Designation` varchar(100) DEFAULT NULL,
  `guardian1_nameofcompany` varchar(100) DEFAULT NULL,
  `guardian1_WorkLocation` varchar(100) DEFAULT NULL,
  `guardian1_Approx_Annual_Income` double DEFAULT NULL,
  `guardian1_Telephone` varchar(100) DEFAULT NULL,
  `guardian1_Fax` varchar(100) DEFAULT NULL,
  `guardian1_Email` varchar(100) DEFAULT NULL,
  `guardian1_Mobile` varchar(100) DEFAULT NULL,
  `guardian2_Relationship_with_child` varchar(100) DEFAULT NULL,
  `guardian2_Name` varchar(100) DEFAULT NULL,
  `guardian2_last` varchar(100) DEFAULT NULL,
  `guardian2_DateofBirth` varchar(100) DEFAULT NULL,
  `guardian2_Qualification` varchar(100) DEFAULT NULL,
  `guardian2_Occupation` varchar(100) DEFAULT NULL,
  `guardian2_Designation` varchar(100) DEFAULT NULL,
  `guardian2_nameofCompany` varchar(100) DEFAULT NULL,
  `guardian2_WorkingLocation` varchar(100) DEFAULT NULL,
  `guardian2_Telephone` varchar(100) DEFAULT NULL,
  `guardian2_Fax` varchar(100) DEFAULT NULL,
  `guardian2_Email` varchar(100) DEFAULT NULL,
  `guardian2_Mobile` varchar(100) DEFAULT NULL,
  `guardian2_Approximate_Annual_salary` double DEFAULT NULL,
  `require_foodService` varchar(10) DEFAULT NULL,
  `require_Transport` varchar(10) DEFAULT NULL,
  `learning disability` varchar(10) DEFAULT NULL,
  `special_Requirement` varchar(500) DEFAULT NULL,
  `Child_Sibling1_firstname` varchar(100) DEFAULT NULL,
  `Child_Sibling1_lastname` varchar(100) DEFAULT NULL,
  `Child_Sibling1_age` int(10) DEFAULT NULL,
  `Child_Sibling1_education` varchar(100) DEFAULT NULL,
  `Child_Sibling1_nameofSchool` varchar(100) DEFAULT NULL,
  `Child_Sibling2_firstname` varchar(100) DEFAULT NULL,
  `Child_Sibling2_lastname` varchar(100) DEFAULT NULL,
  `Child_Sibling2_age` int(10) DEFAULT NULL,
  `Child_Sibling2_education` varchar(100) DEFAULT NULL,
  `Child_Sibling2_nameofschool` varchar(100) DEFAULT NULL,
  `student_info_id` bigint(20) DEFAULT NULL,
  `ECI_NameofContactPerson` varchar(100) DEFAULT NULL,
  `ECI_address` varchar(100) DEFAULT NULL,
  `ECI_RelationwithChild` varchar(100) DEFAULT NULL,
  `ECI_Telephoneno` varchar(100) DEFAULT NULL,
  `ECI_Mobileno` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK46DC4705E4087D4` (`student_info_id`),
  CONSTRAINT `FK46DC4705E4087D4` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `student_admission_ibfk_1` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `student_admission` */

insert  into `student_admission`(`id`,`child_name`,`child_last_name`,`seeking_grade`,`academic_year`,`date_of_birth`,`palec_of_birth`,`sex`,`mother_tongue`,`religion`,`nationality`,`ci_ResidentalAddress`,`ci_city`,`ci_postalcode`,`ci_landmark`,`ci_state`,`ci_telephoneno`,`ci_country`,`dlsa_Name_of_school`,`dlsa_Curriculum_Board`,`dlsa_grade_class`,`dlsa_state`,`dlsa_city`,`dlsa_percentageofmarks`,`dlsa_postalcode`,`dlsa_address`,`dlsa_postal`,`dlsa_country`,`dlsa_telephone`,`dlsa_reason_for_change`,`dlsa_grade_repeat`,`guardian1_Relationship_with_child`,`guardian1_Name`,`guardian1_last`,`guardian1_DateofBirth`,`guardian1_Qualification`,`guardian1_Occupation`,`guardian1_Designation`,`guardian1_nameofcompany`,`guardian1_WorkLocation`,`guardian1_Approx_Annual_Income`,`guardian1_Telephone`,`guardian1_Fax`,`guardian1_Email`,`guardian1_Mobile`,`guardian2_Relationship_with_child`,`guardian2_Name`,`guardian2_last`,`guardian2_DateofBirth`,`guardian2_Qualification`,`guardian2_Occupation`,`guardian2_Designation`,`guardian2_nameofCompany`,`guardian2_WorkingLocation`,`guardian2_Telephone`,`guardian2_Fax`,`guardian2_Email`,`guardian2_Mobile`,`guardian2_Approximate_Annual_salary`,`require_foodService`,`require_Transport`,`learning disability`,`special_Requirement`,`Child_Sibling1_firstname`,`Child_Sibling1_lastname`,`Child_Sibling1_age`,`Child_Sibling1_education`,`Child_Sibling1_nameofSchool`,`Child_Sibling2_firstname`,`Child_Sibling2_lastname`,`Child_Sibling2_age`,`Child_Sibling2_education`,`Child_Sibling2_nameofschool`,`student_info_id`,`ECI_NameofContactPerson`,`ECI_address`,`ECI_RelationwithChild`,`ECI_Telephoneno`,`ECI_Mobileno`) values (6,'Shyam','Sharma','Early Years 1','2014-2015','02-02-1986','Mangalore','Male','Engish','hindu','Indian','#57 Lane Road H.S.R Layout Main Road Bangalore','Bangalore','560102','KFC','karnataka','9008365318','Bangalore','DV school','CBSE','Montessori 1','karnakata','bangalore','60%','560102','HSR Layout','560102','India','90083653147','not better than this','Montessori 2','Father','shyam','sahu','02/02/1986','BCA','Railway Worker','Helper','ETS ','Kolkatta',26000,'(900) 836-5317','56010245','56010245','9008365317','Mother','raji ','sahu','02/02/1986','BCA','Railway Worker','Helper','ETS','Kolkatta','(900) 836-5317','56010245','shyam.citech@gmial.com','9008365317',440000,'YES','YES','YES','NO New Requirements are Needes','Harish','sahu',14,'MCA','Cambridge Institute of Technology','manshi','sahu',15,'MCA','Cambridge Institute of Technology',2,'shyam','dadasd','Father','(900) 836-5317','(900) 836-5317'),(7,'Ram','Sharma','Early Years 1','2014-2015','02/02/1986','A.P','Female','Engish','hindu','Indian','#43 kalavari Residency outerring Road Bangalore','Mangalore','560102','#45 kalavari Residency house No 45','karnataka','9008365319','Mangalore','DV school','CBSE','Montessori 1','karnakata','bangalore','60%','560102','HSR Layout','560102','India','90083653147','not better than this','Montessori 2','mother',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,NULL,NULL,NULL,NULL),(8,'Raja','Sharma','Early Years 1','2014-2015','02/02/1986','Mumbai','Female','Engish','hindu','Indian','#301 Sri Venkataswaraya PG outerring Road Bangalore','Pune','560102','#103 OuterRing road Marathalli','karnataka','9008365317','India','DV school','CBSE','Montessori 1','karnakata','bangalore','60%','560102','HSR Layout','560102','India','90083653147','not better than this','Montessori 2','uncle','',NULL,'','','','','','',0,'','','','','','',NULL,'','','','','','','','','','',0,NULL,NULL,NULL,NULL,'','',0,'','','','',0,'','',6,'','','','',''),(9,'Jagdish','Panchal','Early Years 1','2014-2015','02/02/1986','Chennai','Female','Engish','hindu','Indian','#405 Agra Sector 1 Bangalore','Delhi','560102','HSR Layour #45 Street Road ','karnataka','9008365317','India','DV school','CBSE','Montessori 1','karnakata','bangalore','60%','560102','HSR Layout','560102','India','90083653147','not better than this','Montessori 2','mother','',NULL,'','','','','','',0,'','','','','','',NULL,'','','','','','','','','','',0,NULL,NULL,NULL,NULL,'','',0,'','','','',0,'','',7,'','','','',''),(10,'Shyam','Sharma','Early Years 1','2014-2015','03/02/1987','Delhi','Male','Engish','hindu','Indian','#45 Nil Giri Residency outerring Road Bangalore','Mangalore','560102','#43 Market place near Forum Mall','karnataka','9008365317','Bangalore','KV school','CBSE','Montessori 1','karnakata','bangalore','60%','560102','HSR Layout','560102','India','90083653147','not better than this','Montessori 2','Father','shyam',NULL,'02/02/1986','Railway Worker','Railway Worker','Helper','ETS ','Kolkatta',26000,'(900) 836-5317','56010245','56010245','9008365317','Mother','raji ','sahu','02/02/1986','BCA','Railway Worker','Helper','ETS','Kolkatta','(900) 836-5317','56010245','shyam.citech@gmial.com','9008365317',440000,NULL,NULL,NULL,NULL,'Harish','sahu',14,'MCA','Cambridge Institute of Technology','manshi','sahu',15,'MCA','Cambridge Institute of Technology',2,'shyam','dadasd','Father','(900) 836-5317','(900) 836-5317');

/*Table structure for table `student_attendance_table` */

CREATE TABLE `student_attendance_table` (
  `student_attendance_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `month` varchar(15) DEFAULT NULL,
  `d1` varchar(10) DEFAULT NULL,
  `d2` varchar(10) DEFAULT NULL,
  `d3` varchar(10) DEFAULT NULL,
  `d4` varchar(10) DEFAULT NULL,
  `d5` varchar(10) DEFAULT NULL,
  `d6` varchar(10) DEFAULT NULL,
  `d7` varchar(10) DEFAULT NULL,
  `d8` varchar(10) DEFAULT NULL,
  `d9` varchar(10) DEFAULT NULL,
  `d10` varchar(10) DEFAULT NULL,
  `d11` varchar(10) DEFAULT NULL,
  `d12` varchar(10) DEFAULT NULL,
  `d13` varchar(10) DEFAULT NULL,
  `d14` varchar(10) DEFAULT NULL,
  `d15` varchar(10) DEFAULT NULL,
  `d16` varchar(10) DEFAULT NULL,
  `d17` varchar(10) DEFAULT NULL,
  `d18` varchar(10) DEFAULT NULL,
  `d19` varchar(10) DEFAULT NULL,
  `d20` varchar(10) DEFAULT NULL,
  `d21` varchar(10) DEFAULT NULL,
  `d22` varchar(10) DEFAULT NULL,
  `d23` varchar(10) DEFAULT NULL,
  `d24` varchar(10) DEFAULT NULL,
  `d25` varchar(10) DEFAULT NULL,
  `d26` varchar(10) DEFAULT NULL,
  `d27` varchar(10) DEFAULT NULL,
  `d28` varchar(10) DEFAULT NULL,
  `d29` varchar(10) DEFAULT NULL,
  `d30` varchar(10) DEFAULT NULL,
  `d31` varchar(10) DEFAULT NULL,
  `year` varchar(10) DEFAULT NULL,
  `student_info_id` bigint(20) DEFAULT NULL,
  `student_name` varchar(50) DEFAULT NULL,
  `student_standard` varchar(20) DEFAULT NULL,
  `student_section` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`student_attendance_id`),
  KEY `student_attendance_table_ibfk_1` (`student_info_id`),
  KEY `FKF9D2C7CE4087D4` (`student_info_id`),
  KEY `FKF9D2C7C72706C79` (`student_info_id`),
  CONSTRAINT `FKF9D2C7C72706C79` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `student_attendance_table_ibfk_1` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

/*Data for the table `student_attendance_table` */

insert  into `student_attendance_table`(`student_attendance_id`,`month`,`d1`,`d2`,`d3`,`d4`,`d5`,`d6`,`d7`,`d8`,`d9`,`d10`,`d11`,`d12`,`d13`,`d14`,`d15`,`d16`,`d17`,`d18`,`d19`,`d20`,`d21`,`d22`,`d23`,`d24`,`d25`,`d26`,`d27`,`d28`,`d29`,`d30`,`d31`,`year`,`student_info_id`,`student_name`,`student_standard`,`student_section`) values (69,'Mar','S','P','A','H','P','A','P','S','P','A','P','P','P','A','P','A','P','A','A','P','A','S','S','A','P','A','P','A','P','A','H','2015',2,'Mohan','A','Class I'),(70,'Mar','S','P','A','H','P','A','P','S','P','A','P','P','P','A','P','A','P','A','A','P','A','S','S','A','P','A','P','A','P','A','H','2015',3,'Khader','B','Class I'),(71,'Mar','S','P','A','H','P','A','A','S','P','A','P','P','P','A','P','A','P','A','A','P','A','S','S','A','P','A','P','A','P','A','H','2015',4,'Harish','A','CLASS II'),(72,'Mar','S','P','A','H','P','A','P','S','P','A','P','P','P','A','P','A','P','A','A','P','A','S','H','A','P','A','P','A','P','A','H','2015',5,'Sunny','A','Class III'),(74,'Mar','S','P','A','H','P','A','H','S','P','P','A','P','P','A','P','A','P','A','A','P','A','S','A','A','P','A','P','A','P','A','H','2015',7,NULL,NULL,NULL),(75,'Mar','S','P','A','H','P','A','P','S','H','P','A','P','P','A','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',8,NULL,NULL,NULL),(76,'Mar','S','P','A','H','P','A','A','S','S','P','A','P','P','A','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',9,NULL,NULL,NULL),(77,'Apr','P','A','A','H','P','A','A','P','P','P','A','P','P','A','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',2,NULL,NULL,NULL),(78,'Apr','P','A','A','H','P','A','A','P','A','P','A','P','P','S','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',3,NULL,NULL,NULL),(79,'Apr','P','S','A','H','P','A','A','P','H','P','A','P','P','A','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',4,NULL,NULL,NULL),(80,'Apr','P','S','A','H','P','A','A','P','A','P','A','P','P','A','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',5,NULL,NULL,NULL),(81,'Apr','P','S','A','H','P','A','A','P','S','P','P','P','P','A','P','A','P','A','A','P','A','S','A','A','P','S','P','A','P','A','H','2015',6,NULL,NULL,NULL),(82,'Apr','P','S','A','H','P','A','A','P','S','P','P','P','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','P','A','H','2015',7,NULL,NULL,NULL),(83,'Apr','P','S','A','H','P','A','P','P','P','P','P','P','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','P','A','H','2015',8,NULL,NULL,NULL),(84,'Apr','P','S','A','H','P','A','P','P','P','P','P','P','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','P','A','H','2015',9,NULL,NULL,NULL),(85,'May','P','S','A','H','P','A','P','P','P','P','P','P','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','H','2015',2,NULL,NULL,NULL),(86,'May','P','H','A','H','P','A','P','P','P','P','P','P','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','H','2015',3,NULL,NULL,NULL),(87,'May','S','H','A','H','P','A','P','P','A','P','A','A','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','H','2015',4,NULL,NULL,NULL),(88,'May','P','H','A','H','P','A','P','P','A','P','A','A','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','H','2015',5,NULL,NULL,NULL),(89,'May','P','H','A','H','P','A','P','P','A','P','A','A','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','H','2015',6,NULL,NULL,NULL),(90,'May','P','H','A','H','P','A','P','P','A','P','A','S','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','P','2015',7,NULL,NULL,NULL),(91,'May','P','H','A','H','P','A','P','P','A','P','A','S','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','A','S','A','H','2015',8,NULL,NULL,NULL),(92,'May','P','H','A','H','P','A','P','P','A','P','A','S','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','S','S','A','P','2015',9,NULL,NULL,NULL),(93,'May','P','H','A','H','P','A','P','P','A','P','A','S','P','S','P','A','P','A','A','A','A','S','A','A','P','S','P','S','S','A','P','2015',2,NULL,NULL,NULL),(94,'May','P','H','A','H','P','A','P','P','A','P','A','S','P','S','P','A','P','P','A','A','A','S','A','A','P','S','P','S','S','A','P','2015',3,NULL,NULL,NULL),(95,'May','P','H','A','H','P','A','P','P','A','P','A','S','P','S','P','A','P','P','A','A','A','S','A','A','P','H','P','S','S','S','P','2015',4,NULL,NULL,NULL),(96,'May','P','H','A','H','P','A','P','H','A','P','A','S','P','S','P','A','P','P','S','A','A','S','A','A','P','H','P','S','S','S','P','2015',5,NULL,NULL,NULL),(97,'May','P','H','A','H','P','A','P','H','A','P','A','S','P','S','P','A','P','P','A','A','A','S','A','A','P','H','P','S','S','S','P','2015',6,NULL,NULL,NULL),(98,'May','P','H','A','H','P','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','A','P','H','P','S','S','S','P','2015',7,NULL,NULL,NULL),(99,'May','P','H','A','H','A','A','P','A','A','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','P','S','S','S','S','2015',8,NULL,NULL,NULL),(100,'May','P','H','A','H','A','A','P','A','A','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','P','S','S','S','S','2015',9,NULL,NULL,NULL),(101,'Jun','P','H','A','H','A','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','P','S','S','S','S','2015',2,NULL,NULL,NULL),(102,'Jun','P','H','A','H','A','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','P','S','S','S','S','2015',3,NULL,NULL,NULL),(103,'Jun','P','H','A','H','A','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','A','S','S','S','S','2015',4,NULL,NULL,NULL),(104,'Jun','P','H','A','H','A','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','A','S','S','S','S','2015',5,NULL,NULL,NULL),(105,'Jun','P','H','A','H','A','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','A','S','S','S','S','2015',6,NULL,NULL,NULL),(106,'Jun','P','H','A','H','A','A','P','A','H','P','A','S','P','S','P','A','P','P','S','A','A','S','A','S','P','H','A','S','S','S','S','2015',7,NULL,NULL,NULL),(107,'Jun','A','H','A','H','A','A','P','A','H','H','A','S','P','H','P','A','P','P','S','A','A','S','A','S','P','H','A','S','A','S','S','2015',8,NULL,NULL,NULL),(108,'Jun','A','H','A','H','A','A','P','A','H','H','A','S','P','H','P','A','P','P','S','A','A','S','A','S','P','H','A','S','A','S','S','2015',9,NULL,NULL,NULL),(109,'May','A','H','A','P','A','A','P','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','S','P','H','A','S','S','S','S','2015',2,NULL,NULL,NULL),(110,'May','A','H','A','P','A','A','P','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','S','P','H','A','S','S','S','S','2015',3,NULL,NULL,NULL),(111,'May','A','H','A','P','A','A','P','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','S','P','H','A','S','A','S','S','2015',4,NULL,NULL,NULL),(112,'May','A','H','A','P','A','A','S','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','A','P','H','A','S','A','S','S','2015',5,NULL,NULL,NULL),(113,'May','A','H','A','P','A','A','S','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','S','P','H','A','S','A','S','S','2015',6,NULL,NULL,NULL),(114,'May','A','P','A','P','H','A','S','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','A','P','H','A','S','A','S','S','2015',7,NULL,NULL,NULL),(115,'May','A','H','A','P','H','A','S','A','H','H','A','S','P','H','P','A','P','P','S','A','A','P','A','A','P','S','A','S','A','S','S','2015',8,NULL,NULL,NULL),(116,'May','A','H','A','P','H','A','S','A','H','H','A','S','S','H','P','A','P','P','S','A','A','P','A','A','P','S','A','S','A','S','S','2015',9,NULL,NULL,NULL),(117,'Apr','A','P','A','P','H','A','S','A','H','H','A','S','S','H','P','A','S','P','S','A','A','P','A','A','P','S','A','S','A','S','P','2015',2,NULL,NULL,NULL),(118,'Apr','A','P','A','P','H','A','S','A','H','H','A','S','S','H','P','A','S','P','S','A','A','P','A','A','P','S','A','S','A','P','P','2015',3,NULL,NULL,NULL),(119,'Apr','A','P','A','P','H','A','S','A','H','H','A','S','S','H','P','A','S','P','S','A','A','P','A','A','P','S','A','S','A','P','P','2015',4,NULL,NULL,NULL),(120,'Apr','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','P','S','A','A','P','A','A','P','S','A','S','A','P','P','2015',5,NULL,NULL,NULL),(121,'Apr','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','P','S','A','A','P','A','A','P','S','A','S','A','P','P','2015',6,NULL,NULL,NULL),(122,'Apr','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','P','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',7,NULL,NULL,NULL),(123,'Apr','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',8,NULL,NULL,NULL),(124,'Apr','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',9,NULL,NULL,NULL),(125,'Jun','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',2,NULL,NULL,NULL),(126,'Jun','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',3,NULL,NULL,NULL),(127,'Jun','A','P','A','P','H','A','S','A','H','H','A','A','S','H','P','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',4,NULL,NULL,NULL),(128,'Jun','A','P','A','P','P','A','S','A','H','H','A','A','S','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',5,NULL,NULL,NULL),(129,'Jun','A','P','A','P','H','A','S','A','H','H','A','A','S','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',6,NULL,NULL,NULL),(130,'Jun','A','P','A','P','P','A','S','A','H','H','A','A','S','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','P','2015',7,NULL,NULL,NULL),(131,'Jun','S','P','A','P','P','A','S','A','H','H','A','A','S','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','A','2015',8,NULL,NULL,NULL),(132,'Jun','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','A','2015',9,NULL,NULL,NULL),(133,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','A','2015',2,NULL,NULL,NULL),(134,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','S','A','S','A','A','P','A','A','P','S','A','H','A','P','A','2015',3,NULL,NULL,NULL),(135,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','A','A','S','A','A','P','A','A','P','S','A','H','A','P','A','2015',4,NULL,NULL,NULL),(136,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','A','A','S','A','A','P','A','A','P','S','A','H','A','P','A','2015',5,NULL,NULL,NULL),(137,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','A','A','S','A','A','A','A','A','P','S','A','H','A','P','A','2015',6,NULL,NULL,NULL),(138,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','A','S','S','A','A','A','P','A','P','S','A','H','A','P','A','2015',7,NULL,NULL,NULL),(139,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','A','S','S','A','A','A','P','A','P','S','A','H','A','P','A','2015',8,NULL,NULL,NULL),(140,'Jul','S','P','A','P','P','A','S','A','H','H','A','A','A','H','A','A','A','S','S','A','A','A','P','A','P','S','S','H','A','P','A','2015',9,NULL,NULL,NULL),(157,'Aug','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',2,NULL,NULL,NULL),(158,'Aug','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',3,NULL,NULL,NULL),(159,'Aug','P','H','A',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,NULL,NULL,NULL,NULL,NULL,'S',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',4,NULL,NULL,NULL),(160,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',2,NULL,NULL,NULL),(161,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',3,NULL,NULL,NULL),(162,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',4,NULL,NULL,NULL),(163,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',5,NULL,NULL,NULL),(164,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',6,NULL,NULL,NULL),(165,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',7,NULL,NULL,NULL),(166,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',8,NULL,NULL,NULL),(167,'May',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',9,NULL,NULL,NULL),(168,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',2,NULL,NULL,NULL),(169,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',3,NULL,NULL,NULL),(170,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',4,NULL,NULL,NULL),(171,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',5,NULL,NULL,NULL),(172,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',6,NULL,NULL,NULL),(173,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',7,NULL,NULL,NULL),(174,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',8,NULL,NULL,NULL),(175,'Jun',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',9,NULL,NULL,NULL),(176,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',2,NULL,NULL,NULL),(177,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',3,NULL,NULL,NULL),(178,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',4,NULL,NULL,NULL),(179,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',5,NULL,NULL,NULL),(180,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',6,NULL,NULL,NULL),(181,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',7,NULL,NULL,NULL),(182,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',8,NULL,NULL,NULL),(183,'Jul',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2015',9,NULL,NULL,NULL);

/*Table structure for table `student_book_issued_details` */

CREATE TABLE `student_book_issued_details` (
  `slno` bigint(40) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(40) DEFAULT NULL,
  `section_name` varchar(40) DEFAULT NULL,
  `student_name` varchar(40) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `no_of_days` bigint(40) DEFAULT NULL,
  `book_id` varchar(40) DEFAULT NULL,
  `book_category_name` varchar(40) DEFAULT NULL,
  `book_title` varchar(40) DEFAULT NULL,
  `book_author_name` varchar(40) DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  PRIMARY KEY (`slno`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;

/*Data for the table `student_book_issued_details` */

insert  into `student_book_issued_details`(`slno`,`class_name`,`section_name`,`student_name`,`due_date`,`no_of_days`,`book_id`,`book_category_name`,`book_title`,`book_author_name`,`issued_date`) values (61,'Class I','A','Santosh','2015-04-21',7,'b222','Social','C','David Sne',NULL),(62,'Class I','A','Santosh','2015-04-21',7,'b123','Sceince','Java','santosh kiran',NULL),(80,'Class I','A','R.Santosh','2015-05-02',7,'b123','Sceince','Java','santosh kiran',NULL),(85,'Class I','A','R.Santosh','2015-04-20',7,'b123','Sceince','Java','santosh kiran',NULL),(87,'Class I','A','R.Santosh','2015-05-02',7,'b222','Social','C','David Sne',NULL),(88,'Class I','A','Santosh','2015-05-02',7,'b123','Sceince','Java','santosh kiran',NULL),(113,'Class I','A','Mohan Kumar','2015-04-23',7,'b123','Sceince','Java','santosh kiran',NULL),(115,'Class I','A','Mohan Kumar','2015-05-03',7,'b123','Sceince','Java','santosh kiran',NULL),(116,'Class I','A','Mohan Kumar','2015-05-03',7,'b123','Sceince','Java','santosh kiran',NULL),(117,'Class I','A','Mohan','2015-05-20',7,'b123','Sceince','Java','santosh kiran',NULL),(119,'Class I','A','Mohan','2015-06-03',7,'b222','Social','C','David Sne',NULL),(121,'Class I','A','Student','2015-09-22',7,'B129','Biology','Computer General','Nancy Buckingham',NULL),(122,'Class I','A','KHADER','2015-09-22',7,'B129','Biology','Computer General','Nancy Buckingham',NULL),(123,'Class I','A','Student','2015-09-22',7,'B115','Business','JAVA','Nancy Buckingham',NULL);

/*Table structure for table `student_inbox_table` */

CREATE TABLE `student_inbox_table` (
  `student_inbox_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_message` varchar(500) DEFAULT NULL,
  `student_info_id` bigint(20) DEFAULT NULL,
  `inbox_subject` varchar(100) DEFAULT NULL,
  `sender_name` varchar(100) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `sender_designation` varchar(20) DEFAULT NULL,
  `sender_standard` varchar(20) DEFAULT NULL,
  `sender_section` varchar(10) DEFAULT NULL,
  `sender_subject` varchar(20) DEFAULT NULL,
  `sender_id` int(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`student_inbox_id`),
  KEY `student_info_id` (`student_info_id`),
  KEY `FK2A33A4D1E4087D4` (`student_info_id`),
  KEY `FK2A33A4D172706C79` (`student_info_id`),
  CONSTRAINT `FK2A33A4D172706C79` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `student_inbox_table_ibfk_1` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `student_inbox_table` */

insert  into `student_inbox_table`(`student_inbox_id`,`student_message`,`student_info_id`,`inbox_subject`,`sender_name`,`date`,`sender_designation`,`sender_standard`,`sender_section`,`sender_subject`,`sender_id`,`status`) values (4,'hi to what5 jktbvbgr ghfhggj',3,'FeedBack From Staff','Srinivas - Principal','Nov-26-2013 11:24:59','staff','4','B','Principal',1,1),(7,'ghjkogcgh  cvb cbh g',3,'FeedBack From Staff','Srinivas - Principal','Nov-26-2013 12:16:56','staff','4','B','Principal',1,1),(9,'You Marks is Too Low in Maths Please Try to Improve It',2,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:12:36','staff','4','B','Principal',1,0),(40,'You Marks is Too Low in Maths Please Try to Improve It',2,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:12:36','staff','4','B','Principal',1,0),(41,'You Marks is Too Low in Maths Please Try to Improve It',2,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:12:36','staff','4','B','Principal',1,0),(42,'You Marks is Too Low in Maths Please Try to Improve It',2,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:12:36','staff','4','B','Principal',1,0),(43,'You Marks is Too Low in Maths Please Try to Improve It',2,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:12:36','staff','4','B','Principal',1,0),(44,'You Marks is Too Low in Maths Please Try to Improve It',2,'feedback from staff','Srinivas - Principal','Sep-08-2015 13:12:36','staff','4','B','Principal',1,0);

/*Table structure for table `student_info_table` */

CREATE TABLE `student_info_table` (
  `student_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_uniqueId` varchar(100) DEFAULT NULL,
  `student_name` varchar(50) DEFAULT NULL,
  `student_dob` date DEFAULT NULL,
  `student_address` varchar(100) DEFAULT NULL,
  `student_doj` date DEFAULT NULL,
  `student_section` varchar(20) DEFAULT NULL,
  `student_user_name` varchar(20) DEFAULT NULL,
  `student_password` varchar(20) DEFAULT NULL,
  `parent_info_id` bigint(20) DEFAULT NULL,
  `student_gender` varchar(10) DEFAULT NULL,
  `student_standard` varchar(11) DEFAULT NULL,
  `student_sir_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`student_id`),
  KEY `parent_info_id` (`parent_info_id`),
  KEY `FK45CAF981494DC146` (`parent_info_id`),
  KEY `FK45CAF981D8EC3401` (`parent_info_id`),
  CONSTRAINT `FK45CAF981D8EC3401` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`),
  CONSTRAINT `student_info_table_ibfk_1` FOREIGN KEY (`parent_info_id`) REFERENCES `parent_info_table` (`parent_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `student_info_table` */

insert  into `student_info_table`(`student_id`,`student_uniqueId`,`student_name`,`student_dob`,`student_address`,`student_doj`,`student_section`,`student_user_name`,`student_password`,`parent_info_id`,`student_gender`,`student_standard`,`student_sir_name`) values (2,'T201000003','Student','2010-01-21','VijayPG(Bangalore)','2013-01-08','A','student','student',1,'male','Class I','parentA'),(3,'T201000005','KHADER','2009-01-21','WhiteField(Bangalore)','2013-01-08','B','student2','student2',3,'male','Class I','parent2'),(4,'T2010000031','Harish','1999-01-01','BTM Bangalore','2012-01-01','A','student1','student1',4,'Male','Class II','upreti'),(5,'T201000004','Sunny','2015-02-25','BTM Bangalore','2015-02-25','A','santosh_sahu','123456',1,'Male','Class III','SHARMA'),(6,'T201000008','T.Santosh','2015-02-26','BTM Bangalore','2015-02-26','a','santosh.citech@gmail','123456',1,'Male','Class III','upreti'),(7,'T201000006','RAM','2015-02-27','BTM Bangalore','2015-02-27','A','santosh.citech@gmail','123456',1,'Male','Class IV','kumar'),(8,'T201000009','R.Santosh','2015-03-07','bangalore','2015-03-07','A','santosh.citech@gmail','santosh',1,'Male','Class I','VERMA'),(9,'T2010000010','SANTOSH','2015-03-08','Mangalore','2015-03-08','C','santosh.citech@gmail','santosh123456',1,'Male','Class I','Verma');

/*Table structure for table `student_photos_table` */

CREATE TABLE `student_photos_table` (
  `student_photo_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` mediumtext,
  `student_id` bigint(30) DEFAULT NULL,
  PRIMARY KEY (`student_photo_id`),
  KEY `student_id` (`student_id`),
  KEY `FK7A18A2143210A52B` (`student_id`),
  KEY `FK7A18A214964089D0` (`student_id`),
  CONSTRAINT `FK7A18A214964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `student_photos_table_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;

/*Data for the table `student_photos_table` */

insert  into `student_photos_table`(`student_photo_id`,`image`,`student_id`) values (147,'3D-Shree-Ganesh-Chaturthi-HD-Wallpaper-Free.jpg',2),(148,'1917_om-wallpaper-hd-28.jpg',2),(149,'2144_angry-hanuman-wallpaper-02.jpg',2),(150,'1360057.jpg',2),(151,'1569515.jpg',2),(152,'Chrysanthemum.jpg',2),(153,'Hydrangeas.jpg',2),(154,'Jellyfish.jpg',2),(155,'Tulips.jpg',2),(157,'Koala.jpg',2),(158,'Hydrangeas.jpg',2),(159,'Chrysanthemum.jpg',2),(160,'Desert.jpg',2),(161,'Hydrangeas.jpg',2),(162,'Jellyfish.jpg',2),(163,'Koala.jpg',2),(164,'Penguins.jpg',2),(165,'Tulips.jpg',2),(166,'3D-Shree-Ganesh-Chaturthi-HD-Wallpaper-Free.jpg',2),(167,'1917_om-wallpaper-hd-28.jpg',2),(168,'2144_angry-hanuman-wallpaper-02.jpg',2),(169,'1360057.jpg',2),(170,'1569515.jpg',2),(171,'1604558.jpg',2);

/*Table structure for table `student_sent_items_table` */

CREATE TABLE `student_sent_items_table` (
  `student_sent_items_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sent_items_message` varchar(500) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `sent_items_subject` varchar(100) DEFAULT NULL,
  `sender_name` varchar(20) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `sender_designation` varchar(20) DEFAULT NULL,
  `sender_standard` varchar(20) DEFAULT NULL,
  `sender_section` varchar(10) DEFAULT NULL,
  `sender_subject` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`student_sent_items_id`),
  KEY `staff_info_id` (`student_id`),
  KEY `FKA9FB7DEC3210A52B` (`student_id`),
  KEY `FKA9FB7DEC964089D0` (`student_id`),
  CONSTRAINT `FKA9FB7DEC964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `student_sent_items_table_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `student_sent_items_table` */

insert  into `student_sent_items_table`(`student_sent_items_id`,`sent_items_message`,`student_id`,`sent_items_subject`,`sender_name`,`date`,`sender_designation`,`sender_standard`,`sender_section`,`sender_subject`,`status`) values (2,'hi i am fine who is these',2,'feedback to staff','Srinivas','Nov-19-2013 11:34:42','Class Teacher','5','A','Maths',0),(3,'bhjhjbhjbjjbhj vhbhjbjbhjbbhb',2,'feedback to staff','Class Teacher','Nov-19-2013 14:11:22','Class Teacher','5','A','Maths',0),(5,'ghd bn h h jk jj kjklkl;kl',2,'feedback to staff','Class Teacher','Nov-28-2013 13:11:27','Class Teacher','','','Maths',0),(6,'gfh f df g',2,'feedback to staff','Srinivas','Nov-28-2013 13:27:25','Class Teacher','','','Principal',0),(7,'sfdghnsd',2,'feedback to staff','Subash','Dec-24-2013 13:15:10','Class Teacher','5','A','Maths',0),(8,'xzcdasfsaf',2,'feedback to staff','Srinivas','Jan-25-2014 13:34:23','Class Teacher','5','A','Maths',0),(9,'sdfsdfs',2,'feedback to staff','Class Teacher','Jan-25-2014 13:35:00','Class Teacher','5','A','Maths',0),(10,'hi srinivas how are you',2,'feedback to staff','Srinivas','Jan-18-2015 17:45:46','Class Teacher','5','A','Maths',0),(11,'mmmmmmmmmmmmm',2,'feedback to staff','Subash','Mar-12-2015 22:55:11','Vice Principal','class I','A','Vice',0),(12,'jjjjjjjjjjjjjjjjj',2,'feedback to staff','Subash','Mar-12-2015 22:56:53','Vice Principal','class I','A','Vice',0),(13,'Hi subhash how are your doing today <br>',2,'feedback to staff','Subash','Mar-20-2015 14:38:34','Vice Principal','class I','A','Vice',0),(14,'dddddddddddddddddddddddddddddddddddddddddddddddddddddddd',2,'feedback to staff','Srinivas','Mar-20-2015 15:09:23','Vice Principal','class I','A','Vice',0),(15,'Hai subjash how are your doing <br>',2,'feedback to staff','Subash','Mar-20-2015 15:17:17','Vice Principal','class I','A','Vice',0),(17,'hi sri<br>',2,'feedback to staff','Srinivas','Sep-08-2015 13:54:35','Class Teacher','Class I','A','Maths',0);

/*Table structure for table `subjects_table` */

CREATE TABLE `subjects_table` (
  `subjects_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject1` float DEFAULT '0',
  `subject2` float DEFAULT '0',
  `subject3` float DEFAULT '0',
  `subject4` float DEFAULT '0',
  `subject5` float DEFAULT '0',
  `subject6` float DEFAULT '0',
  `subject7` float DEFAULT '0',
  `subject8` float DEFAULT '0',
  `subject9` float DEFAULT '0',
  `subject10` float DEFAULT '0',
  `subject11` float DEFAULT '0',
  `subject12` float DEFAULT '0',
  `subject13` float DEFAULT '0',
  `subject14` float DEFAULT '0',
  `subject15` float DEFAULT '0',
  `exam_type` varchar(20) DEFAULT NULL,
  `student_info_id` bigint(20) DEFAULT NULL,
  `student_name` varchar(50) DEFAULT NULL,
  `student_standard` varchar(15) DEFAULT NULL,
  `student_section` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subjects_id`),
  KEY `student_info_id` (`student_info_id`),
  KEY `FK46C8FE56E4087D4` (`student_info_id`),
  KEY `FK46C8FE5672706C79` (`student_info_id`),
  CONSTRAINT `FK46C8FE5672706C79` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `subjects_table_ibfk_1` FOREIGN KEY (`student_info_id`) REFERENCES `student_info_table` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Data for the table `subjects_table` */

insert  into `subjects_table`(`subjects_id`,`subject1`,`subject2`,`subject3`,`subject4`,`subject5`,`subject6`,`subject7`,`subject8`,`subject9`,`subject10`,`subject11`,`subject12`,`subject13`,`subject14`,`subject15`,`exam_type`,`student_info_id`,`student_name`,`student_standard`,`student_section`) values (6,56,66,56,65,60,76,76,75,0,0,0,0,0,0,0,'First Term',2,'mohan','5','A'),(7,60,70,70,70,70,70,76,45,0,0,0,0,0,0,0,'Second Term',2,NULL,NULL,NULL),(22,77,99,55,44,33,40,76,25,0,0,0,0,0,0,0,'Second Term',3,NULL,NULL,NULL),(23,26,25,26,25,24,28,28,35,0,0,0,0,0,0,0,'Quaterly',2,NULL,NULL,NULL),(24,24,52,24,62,24,58,35,65,0,0,0,0,0,0,0,'Quaterly',3,NULL,NULL,NULL),(25,30,45,60,50,50,50,75,45,0,0,0,0,0,0,0,'HalfYearly',2,NULL,NULL,NULL),(26,55,66,66,88,88,33,45,78,0,0,0,0,0,0,0,'Annualy',2,NULL,NULL,NULL),(35,50,100,60,40,70,90,30,45,89,35,45,50,45,60,78,'First Term',5,NULL,NULL,NULL);

/*Table structure for table `timetable` */

CREATE TABLE `timetable` (
  `timetable_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`timetable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `timetable` */

/*Table structure for table `transaction_details` */

CREATE TABLE `transaction_details` (
  `tid` bigint(40) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_name` varchar(40) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `transaction_amount` int(40) DEFAULT NULL,
  `transaction_description` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

/*Data for the table `transaction_details` */

insert  into `transaction_details`(`tid`,`transaction_name`,`transaction_date`,`transaction_amount`,`transaction_description`) values (41,'TRAVELLING','2015-03-01',1000,'Petrol Expense'),(42,'TRAVELLING','2015-03-02',500,'Diesel Expense'),(43,'STATIONARY','2015-03-03',200,'Fevistick,Pens,Notebook'),(44,'STATIONARY','2015-03-03',1500,'Notebooks'),(45,'STATIONARY','2015-03-04',1250,'Exam Pads'),(46,'CLEANING ','2015-03-01',100,'Phenyl'),(47,'CLEANING ','2015-03-02',500,'Cleaning Stick'),(48,'FOOD','2015-03-21',500,'Food to examiners'),(49,'FOOD','2015-03-21',200,'Breakfast to staff'),(50,'FOOD','2015-03-25',450,'lunch to staff'),(51,'MISSCELANEOUS','2015-03-25',120,'paid to water guy'),(52,'TRAVELLING','2015-03-28',123,'Petrol expense'),(53,'TRAVELLING','2015-06-09',8900,'Travelling Expenses for shool');

/*Table structure for table `transportation_table` */

CREATE TABLE `transportation_table` (
  `transportation_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `staff_info_id` bigint(20) DEFAULT NULL,
  `student_id` bigint(20) DEFAULT NULL,
  `route_number` varchar(40) DEFAULT NULL,
  `fees` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`transportation_table_id`),
  KEY `FK503D333B3210A52B` (`student_id`),
  KEY `FK503D333BE0194234` (`staff_info_id`),
  KEY `FK503D333B964089D0` (`student_id`),
  KEY `FK503D333B47D40C19` (`staff_info_id`),
  CONSTRAINT `FK503D333B3210A52B` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK503D333B47D40C19` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`),
  CONSTRAINT `FK503D333B964089D0` FOREIGN KEY (`student_id`) REFERENCES `student_info_table` (`student_id`),
  CONSTRAINT `FK503D333BE0194234` FOREIGN KEY (`staff_info_id`) REFERENCES `staff_info_table` (`staff_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transportation_table` */

/*Table structure for table `vehicle_info_table` */

CREATE TABLE `vehicle_info_table` (
  `vehicle_info_table_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vehicle_number` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`vehicle_info_table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `vehicle_info_table` */

/*Table structure for table `video_upload_table` */

CREATE TABLE `video_upload_table` (
  `video_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `video` mediumtext,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `video_upload_table` */

/*Table structure for table `working_days_table` */

CREATE TABLE `working_days_table` (
  `working_days_id` bigint(20) NOT NULL,
  `working_days` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`working_days_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `working_days_table` */

insert  into `working_days_table`(`working_days_id`,`working_days`) values (1,'M-S');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
