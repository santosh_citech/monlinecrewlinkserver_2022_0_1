// tslint:disable-next-line: no-implicit-dependencies
import * as express from 'express';

function authMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
    console.log(`${request.method} ${request.path}`);
    next();
}