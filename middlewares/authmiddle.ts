import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express'
import config from '../config/config';


const verifyToken = (req: Request, res: Response, next: NextFunction) => {
    const token =
        req.body.token || req.query.token || req.headers["x-access-token"] || req.headers["Authorization"];

    if (!token) {
        return res.status(403).send("A token is required for authentication");
    }
    try {
        jwt.verify(token, config.secret, (err: any, decoded: any) => {
            if (err) {
                err = new Error('You are not authenticated!');
                err.status = 401;
                return next(err);
            } else {
                req['decoded'] = decoded;
                req['x-key'] = decoded;
                next();
            }

        });

    } catch (err: any) {
        err = new Error('No token provided!');
        err.status = 403;
        return next(err);
    }
    return next();
};
export {
    verifyToken
}
