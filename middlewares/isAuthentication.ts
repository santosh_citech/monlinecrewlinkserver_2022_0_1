import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import config from '../config/config';

function isAuthentication(req: Request, res: Response, next: NextFunction) {

    var token = (req.body, req.body.token) || (req.query && req.query.token) || req.headers['x-access-token'] || (req.cookies && req.cookies['x-access-token']);
    var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'] || (req.cookies && req.cookies['x-key']);

    if (token || key) {
        jwt.verify(token, config.secret, (err: any, decoded: any) => {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            }
            if (decoded) {   // if everything is good, save to request for use in other routes
                req['decoded'] = decoded;
            }
            next();
        })

    } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }


}
function decodeToken() {

}

export = { isAuthenticationFun: isAuthentication, decodeTokenFun: decodeToken };