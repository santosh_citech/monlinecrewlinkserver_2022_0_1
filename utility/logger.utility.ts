// /**
//  * Configurations of logger.
//  *
//  */
// const winston = require('winston');
// const winstonRotator = require('winston-daily-rotate-file');

// // const consoleConfig = [
// //     new winston.transports.Console({
// //         'colorize': true
// //     })
// // ];

// // const createLogger = winston.createLogger({
// //     transports: [
// //         new winston.transports.Console(),
// //         new winston.transports.File({ filename: 'combined.log' })
// //       ]
// // });

// const successLogger = createLogger;
// successLogger.add(winstonRotator, {
//     'name': 'access-file',
//     'level': 'info',
//     'filename': './logs/access.log',
//     'json': false,
//     'datePattern': 'yyyy-MM-dd-',
//     'prepend': true
// });

// const errorLogger = createLogger;
// errorLogger.add(winstonRotator, {
//     'name': 'error-file',
//     'level': 'error',
//     'filename': './logs/error.log',
//     'json': false,
//     'datePattern': 'yyyy-MM-dd-',
//     'prepend': true
// });

// module.exports = {
//     'successlog': successLogger,
//     'errorlog': errorLogger
// };

// import { Logger, transports } from 'winston';
// const logger = Logger.createLogger({
//     level: 'info',
//     format: winston.format.json(),
//     defaultMeta: { service: 'user-service' },
//     transports: [
//       //
//       // - Write all logs with level `error` and below to `error.log`
//       // - Write all logs with level `info` and below to `combined.log`
//       //
//       new winston.transports.File({ filename: 'error.log', level: 'error' }),
//       new winston.transports.File({ filename: 'combined.log' }),
//     ],
//   });
   