
export default class timeCalc {

    super() {

    }

    static convertDateTimeObjectToNumberUsingMoment(dateTimeObj: object, targe: string) {

    }

   



    static convertDateTimeObjectToNumber(dateTimeObj: any, target: string) {

        if (typeof dateTimeObj === 'undefined' || dateTimeObj === null || typeof dateTimeObj !== 'object') {
            throw new Error("Not valid dateTimeObject passed to convertDateTimeObj()");
        }

        if (dateTimeObj.day === null || dateTimeObj.time === null) {
            throw new Error("Not valid dateTimeObject passed to convertDateTimeObj()");
        }
        if (target == null) {
            target = "";
        }
        target = target.toLowerCase();
        var timeParts = dateTimeObj.time.split(':');
        timeParts[0] = parseInt(timeParts[0]);
        timeParts[1] = parseInt(timeParts[1]);


        if ((timeParts[0] > 23 || timeParts[0] < 0) && (timeParts[1] > 59 || timeParts[1] < 0)) {
            throw new Error("Not valid dateTimeObj.time passed to convertDateTimeObj()");
        }


        let mins = (dateTimeObj.day * 1440) + (timeParts[0] * 60) + (timeParts[1]);

        let result: any = null;
        switch (target) {
            case 'mins':
            case 'min':
            case 'minutes':
            case 'minute':
                result = mins;
                break;
            case 'hrs':
            case 'hr':
            case 'hours':
            case 'hour':
            case 'HOURS':
            case 'HRS':
                result = (mins / 60)//.toFixed(10);
                break;
            case 'days':
            case 'day':
                result = (mins / 1440);
                break;
            default:
                result = mins;
                break;
        }
        return result;

    }


}


