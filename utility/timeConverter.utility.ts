import moment from "moment";

interface Square {
    kind: "square";
    size: number;
}

interface Rectangle {
    kind: "rectangle";
    width: number;
    height: number;
}

interface Circle {
    kind: "circle";
    radius: number;
}

interface userFunctions {
    calculateTimeInToNumber(modelOBJ: { time: string; option: string }): string;
    calculateArea(s: Shape): any;
    getTimeFromMins(mins: any): string;
    calculateExecutionTime(ttime: any): any;
    millisecondsToHHMMYY(ms: any);
}
type Shape = Square | Rectangle | Circle;
export default class timeConverter {


    static totaldistance: number = 0;
    static total: number = 0;
    constructor() {

    }

    static relativeTime(times: any) {

        let message: string = "";


        var timesPart = times.split(":");
        var hh = timesPart[0];
        var mm = timesPart[1];
        var ss = timesPart[2];
        var regExp = /^\d\d:\d\d:\d\d$/

        if (regExp.test(times)) {
            console.log("inside if condition !! for test of a times in : formates ");

        }
        if (
            parseInt(timesPart[0]) >= 0 && parseInt(timesPart[0]) < 25 &&
            parseInt(timesPart[1]) >= 0 && parseInt(timesPart[1]) < 60 &&
            parseInt(timesPart[2]) >= 0 && parseInt(timesPart[2]) < 60) {



            // throw new Error("times is wrong format in calculateSumOfTime()");
            //  return () => Date.now() - start;
            message = "I am inside if condition of relativeTime()";
            return message;
        }



    };



    static calculateTotalDistance(objDistance: number) {

        if (objDistance != 0) {
            this.totaldistance += objDistance;
        }
        return this.totaldistance;
    }

    minsToHHMMSS(n: any) {
        var mins_num = parseInt(n, 10); // don't forget the second param
        var hh: any = Math.floor(mins_num / 60);
        var mm: any = Math.floor((mins_num - ((hh * 3600)) / 60));
        var ss: any = Math.floor((mins_num * 60) - (hh * 3600) - (mm * 60));

        if (hh < 10) { hh = "0" + hh; }
        if (mm < 10) { mm = "0" + mm; }
        if (ss < 10) { ss = "0" + ss; }
        return hh + ':' + mm + ':' + ss;
    }

    static calculateSumOfTime(times: string) {
        var second = (60 * 60) | 3600;
        var minutes = 60;
        var timePieces: any;

        try {

            var time = times.split(':')
            if (time.length !== 2) {
                throw new Error("times is null in calculateSumOfTime()");
            }

            if ((times == null && times == '' && times == 'undefined') || times == null || times == '' || times == 'undefined') {
                throw new Error("times is null in calculateSumOfTime()");

            }


            if (parseInt(time[0]) > 23 || parseInt(time[0]) < 0) {
                throw new Error("hour is in wrong format in calculateSumOfTime()");
            }

            if (parseInt(time[1]) < 0 || parseInt(time[1]) > 59) {
                throw new Error("minutes is in wrong format in calculateSumOfTime()");
            }
            if (parseInt(time[2]) < 0 || parseInt(time[2]) > 59) {
                throw new Error("second is in wrong format in calculateSumOfTime()");
            }


            // var totalseconds = parseInt(time[0]) * second + parseInt(time[1], 10) * minutes + parseInt(time[2]);
            //var totalmin = parseInt(time[0]) * minutes + parseInt(time[1]) + parseInt(time[2]);
            var totalmin = parseInt(time[0]) * minutes + parseInt(time[1]);
            totalmin = Math.floor(totalmin);
            this.total += totalmin;

            return this.total;


        } catch (err: any) {
            return err.message;
        }

    }

    simplifiedMilliseconds(milliseconds: any) {

        const totalSeconds = parseInt("", Math.floor(milliseconds / 1000));
        const totalMinutes = parseInt("", Math.floor(totalSeconds / 60));
        const totalHours = parseInt("", Math.floor(totalMinutes / 60));
        const days = parseInt("", Math.floor(totalHours / 24));

        const seconds = parseInt("", totalSeconds % 60);
        const minutes = parseInt("", totalMinutes % 60);
        const hours = parseInt("", totalHours % 24);

        let time = '1s';
        if (days > 0) {
            time = `${days}d:${hours}h:${minutes}m:${seconds}s`;
        } else if (hours > 0) {
            time = `${hours}h:${minutes}m:${seconds}s`;
        } else if (minutes > 0) {
            time = `${minutes}m:${seconds}s`;
        } else if (seconds > 0) {
            time = `${seconds}s`;
        }
        return time;
    }


    static CalDifferecebetweenTwoTimes(starttime: any, endtime: any) {
        if (starttime == null || starttime == "undefined") {
            throw new Error("starttime is not in correct format in CalDifferecebetweenTwoTimes()");
        }
        if (endtime == null || endtime == "undefined") {
            throw new Error("endtime is not in correct format in CalDifferecebetweenTwoTimes()");
        }
        var timesPart0 = starttime.split(":");
        var hh0 = parseInt(timesPart0[0]);
        var mm0 = parseInt(timesPart0[1]);
        var ss0 = parseInt(timesPart0[2]);

        var timesPart1 = endtime.split(":");
        var hh1 = parseInt(timesPart1[0]);
        var mm1 = parseInt(timesPart1[1]);
        var ss1 = parseInt(timesPart1[2]);

        var diffhour = hh1 - hh0;
        var diffmin = mm1 - mm0;
        var diffsec = ss1 - ss0;
        return diffhour + ":" + diffmin + ":" + diffsec;

    }

    static millisecToHHMMYY(ms: any) {
        // 1- Convert to seconds:
        var msg: any = "";
        let seconds: any = Math.floor(ms / 1000);
        // 2- Extract hours:
        const hours: any = Math.floor(seconds / 3600); // 3,600 seconds in 1 hour
        seconds = Math.floor(seconds % 3600); // seconds remaining after extracting hours

        const minutes: any = Math.floor(seconds / 60); // 60 seconds in 1 minute

        seconds = Math.floor(seconds % 60);
        msg = hours + ":" + minutes + ":" + seconds;
        return msg;
    }
    static calculateExecutionTime(ttime: any): any {

        // var timeDiff = new Date().getTime();
        return this.millisecToHHMMYY(ttime);

    }

    calculateArea(s: Shape) {
        switch (s.kind) {
            case "square": return s.size * s.size;
            case "rectangle": return s.width * s.height;
            case "circle": return Math.PI * s.radius * s.radius;
        }
    }
    calculateTimeInToNumber(modelOBJ: { time: string, day: number, option: string }) {
        if (typeof modelOBJ === 'undefined' || modelOBJ === null || typeof modelOBJ !== 'object') {
            throw new Error("Not valid dateTimeObject passed to calculateTimeInToNumber()");
        }

        if (modelOBJ.day === null || modelOBJ.time === "" || modelOBJ.time === null) {
            throw new Error("Not valid dateTimeObject passed to calculateTimeInToNumber()");
        }
        if (modelOBJ.option == null) {
            modelOBJ.option = "";
        }

        console.log(`Animal moved ${modelOBJ}m.`);
        return "hello world!!!"
    }
    getTimeFromMins(mins: any) {

        if (mins >= 24 * 60 || mins < 0) {
            throw new RangeError("Valid input should be greater than or equal to 0 and less than 1440.");
        }
        var h = (mins / 60) | 0;
        var m = (mins % 60) | 0;
        return moment.utc().hours(h).minutes(m).format("hh:mm");
    };
}