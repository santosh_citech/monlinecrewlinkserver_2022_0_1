import path from 'path';
import PDFDocument from 'pdfkit';
import fs from 'fs';

export default class UtilityConverter {

    static getdays() {
        return ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"]
    }

    static getExten() {
        return [".json", ".pdf", ".cvs", ".xlsx", ".DOCX", ".DOC ", ".xml"];
    }

    static convertToCSV(path: any, filedata: any) {
        let fullpath = ".\\" + path + "\\";
        if (!path) {
            throw console.error("Path missing!!!");

        } if (path) {
            fs.writeFile(fullpath, filedata, "utf8", (err) => {
                (err)
                console.log(err);
                (!err)
                console.log("file has been written successfully !!!");
            });

        }

    }

    static convertToJson = (filename: any, csvdata: any): string => {
        if (typeof csvdata === "undefined" || csvdata === null) {
            throw new Error("data is empty!!")
        }
        fs.stat("./uploads/JSON/" + filename + ".json", function (err, stat) {
            if (err == null) {
                console.log('File exists');
            } else if (err.code === 'ENOENT') {
                fs.writeFile("./uploads/JSON/" + filename + ".json", JSON.stringify(csvdata), "utf8", (err) => {
                    (err)
                    return err;

                });

            } else {
                console.log('Some other error: ', err.code);
            }
        });


        return "file has been written successfully !!!";
    }


    static convertToPDF(csvdata: any, filename: any) {
        if (typeof csvdata === "undefined" || csvdata === null) {
            throw new Error("data is empty!!")
        }

        const doc = new PDFDocument();
        doc.pipe(fs.createWriteStream('.\\uploads\\' + filename + this.getExten()[1]));

        doc.fontSize(15)
            .fillColor('blue')
            .text(JSON.stringify({ result: csvdata, success: true }, null, 2), 100, 100)
        // .link(100, 100, 160, 27, link);

        // doc.pipe("file created successfully");

        doc.end();


    }
    static GetExtension(filename: any, choice: string) {

        switch (choice) {
            case "method1": return filename.substring(filename.lastIndexOf('.') + 1, filename.length) || filename;
            case "method2": return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
            case "method3": return filename.split('.').pop();
            case "method4": return filename.substring(filename.lastIndexOf('.') + 1, filename.length) || filename;
            case "method5": return path.extname(filename).slice(1);
            case "method6":
                var i = filename.lastIndexOf('.');
                return (i < 0) ? '' : filename.substr(i);
                break;

        }

    }

    static GetFileName(filename: any, choice: string) {

        switch (choice) {
            case "method1": return path.parse(filename).name;
            case "method2": return path.parse(filename).ext;
            case "method3": return path.parse(filename).base;
                break;
        }
    }

    static getnthIndex(str: any, pat: any, n: any):number {
        var L = str.length, i = -1;
        while (n-- && i++ < L) {
            i = str.indexOf(pat, i);
            if (i < 0) break;
        }
        return i;
    }


    static hrsToMinutes(time: any): number {
        time = time.split(":");
        return parseInt(time[0]) * 60 + parseInt(time[1]);
    }

    static minutesToHours(minutes: any): string {
        let mins: any = Math.round(parseInt((((minutes / 60) % 1).toFixed(2).substring(2))) * 0.6);
        mins = (mins < 10) ? "0" + mins : mins;
        let hrs: any = Math.floor(minutes / 60);
        hrs = (hrs < 10) ? "0" + hrs : hrs;
        return hrs + ":" + mins;
    }

    static convertInbytes(bytes: any): string {
        let units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
        let i = 0;
        for (i; bytes > 1024; i++) {
            bytes /= 1024;
        }
        return bytes.toFixed(1) + ' ' + units[i]
    }
}
