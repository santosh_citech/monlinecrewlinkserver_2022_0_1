import { PaginateModel, Document, Schema, model } from 'mongoose';
import { paginate } from 'mongoose-paginate-v2';
import mongoosePaginate from 'mongoose-paginate-v2';

interface IStation extends Document {
    slNo: string;
    code: string;
    headstationsignOnTime: number;
    headstationsignoffTime: number;
    name: string;
    noofBeds: number;
    outStationSignOffTime: number,
    markDelete: boolean,
    // createdTime: Date

}



const stSchema = new Schema({
    slNo: String,
    code: String,
    headstationsignOnTime: Number,
    headstationsignoffTime: Number,
    name: String,
    noofBeds: Number,
    outStationSignOnTime: Number,
    outStationSignOffTime: Number,
    markDelete: Boolean,
    // createdTime: Date.now()
},{ timestamps: true });

stSchema.plugin(mongoosePaginate);
// const stationModel = model<IStation, PaginateModel<IStation>>('stations', stSchema);
export default model<IStation, PaginateModel<IStation>>('stations', stSchema);
// export { stationModel }


