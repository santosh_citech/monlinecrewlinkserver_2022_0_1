var mongoose = require('mongoose');
var runningTrainSchema = new mongoose.Schema({
    trainNo: Number,
    startDay: Number,
    createdTime: { type: Date, default: Date.now }
});
const rrModel = mongoose.model('runningTrain', runningTrainSchema);
export { rrModel }

var Schema = mongoose.Schema;