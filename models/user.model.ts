import { Schema, model } from 'mongoose';
import { PaginateModel, Document } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

interface IUser extends Document {
    username: String,
    firstname: String,
    lastname: String,
    password: String,
    email: String,
    remember_token:String,
    rolecode: { type: String, ref: 'role' },
    role_table_id: [{ type: Schema.Types.ObjectId, ref: 'role' }],
    markdelete: { type: Boolean },
    createdtime: { type: Date }

}

const schemaOptions = {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
  };

const userSchema: Schema = new Schema({
    username: String,
    firstname: String,
    lastname: String,
    password: String,
    email: String,
    remember_token:String,
    rolecode: { type: String, ref: 'role', default: 'Planner' },
    role_table_id: [{ type: Schema.Types.ObjectId, ref: 'role' }],
    markdelete: { type: Boolean, default: false },
    createdtime: { type: Date, default: Date.now },
    
    // check for foreign key id of array 
    // fans: [{ type: Schema.Types.ObjectId, ref: 'Person' }] 
},{ timestamps: true });
export default model<IUser, PaginateModel<IUser>>('user', userSchema);
