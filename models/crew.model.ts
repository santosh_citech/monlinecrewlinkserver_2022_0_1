import { Schema, model } from 'mongoose';

const crewSchema: Schema = new Schema({


    slNo: {
        type: Number,
        required: true
    },

    name: {
        type: String,
        required: true
    },


    markDelete: {
        type: Boolean, default: false,
        required: true
    },
    createdTime: {
        type: Date, default: Date.now,
        required: true
    }
},{ timestamps: true });
const crewSchemaModel = model('crewType', crewSchema);

export { crewSchemaModel }