import { Schema, model } from 'mongoose'

const heroSchema: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    alias: {
        type: String,
        required: true
    },
    universe: {
        type: String,
        required: true
    }
})

const heroModel = model('hero', heroSchema)

