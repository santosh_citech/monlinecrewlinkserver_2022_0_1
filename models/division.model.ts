import { PaginateModel, Document, Schema, model } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';


interface IDivision extends Document {
    slNo: Number,
    name: String,
    markDelete: Boolean,
    createdAt: number;
    updatedAt: number

}

const divSchema: Schema = new Schema({
    slNo: Number,
    name: String,
    markDelete: Boolean,
},{ timestamps: true });

divSchema.plugin(mongoosePaginate);
export default model<IDivision, PaginateModel<IDivision>>('divisions', divSchema);
