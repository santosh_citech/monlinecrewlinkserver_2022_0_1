
import { Schema, model } from 'mongoose';

const planSchema: Schema = new Schema({

    planName: String,
    isComplete: { type: Boolean, default: false },
    isUnderReview: { type: Boolean, default: false },
    isLocked: { type: Boolean, default: false },
    coPlanners: [String],
    reviewer: { type: String },
    owner: { type: String },
    isLinkGenerated: { type: Boolean, default: false },
    markDelete: { type: Boolean, default: false },
    createdTime: { type: Date, default: Date.now }


},{ timestamps: true });

const planModel = model('plan', planSchema);

export { planModel }