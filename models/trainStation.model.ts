'use strict'


import { PaginateModel, Document, Schema, model } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';


interface ITrainStations extends Document {
    trainNo: Number,
    stopNo:Number,
    haltNo:Number,
    trainname: String,
    stationCode: String,
    arrivalday: Number,
    departureday: Number,
    arrivalTime: String,
    departureTime: String,
    arrivalInMinutes: Number,
    departureInMinutes:Number,
    arrivalDateTime:Number,
    departureDateTime:Number,
    distance:Number,
    dayOfJourney:Number,
    totalArrivalTime:Number,
    totaldistance:Number,
    locoType:String,
    markDelete:Boolean,
    createdTime: Date,

}

const trainstation: Schema = new Schema({
    trainNo: {
        type: Number,
        required: true
    },
    stopNo: {
        type: Number,
        required: true
    },
    haltNo: {
        type: Number,
        required: false
    },

    stationCode: {
        type: String,
        required: true
    },

    arrivalday: {
        type: Number,
    },
    departureday: {
        type: Number,
    },
    arrivalTime: {
        type: String,
    },
    departureTime: {
        type: String,
    },
    arrivalInMinutes: {
        type: Number

    },
    departureInMinutes: {
        type: Number
    },
    arrivalDateTime: {
        type: Number
    },

    departureDateTime: {
        type: Number
    },

    distance: {
        type: Number,
    },
    dayOfJourney: {
        type: Number,
    },

    totaldistance: {
        type: Number,
        default: 0,
        required: true
    },

    totalArrivalTime: {
        type: Number,
        default: 0
    },
    locoType: {
        type: String,
    },


    markDelete: {
        type: Boolean, default: false,
        required: true
    },
    createdTime: {
        type: Date, default: Date.now,
        required: true
    }
},{ timestamps: true });

trainstation.plugin(mongoosePaginate);
export default model<ITrainStations, PaginateModel<ITrainStations>>('trainstation', trainstation);