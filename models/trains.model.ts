
import { PaginateModel, Document, Schema, model } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';


interface ITrains extends Document {
    trainno: Number,
    trainname: String,
    source: String,
    destination: String,
    runningDays: [Number],
    trainType: String,
    markDelete: Boolean,
    locoType: String,
    createdTime: Date,

}

var trainSchema: Schema = new Schema({
    trainno: Number,
    trainname: String,
    source: String,
    destination: String,
    runningDays: [{ type: Number }],
    trainType: String,
    markDelete: { type: Boolean, default: false },
    locoType: { type: String },
    createdTime: { type: Date, default: Date.now }
},{ timestamps: true });

trainSchema.plugin(mongoosePaginate);
export default model<ITrains, PaginateModel<ITrains>>('trains', trainSchema);
