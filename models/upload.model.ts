import { Schema, model } from 'mongoose';

var UploadSchema: Schema = new Schema({
    fdata: String,
    dataType: String,
    fileType: String,
    originalFileName: String,
    creator: String,
    comments: String,
    isProcessed: { type: Boolean, default: false },
    status: { type: String, default: null },

    markDelete: { type: Boolean, default: false },
    uploadedTime: { type: Date, default: Date.now }
},{ timestamps: true });
const uploadModel = model('Uploads', UploadSchema);
export {
    uploadModel
}