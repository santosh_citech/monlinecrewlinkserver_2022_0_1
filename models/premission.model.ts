import { Schema, model } from 'mongoose';

import { PaginateModel, Document } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';


interface IPremission extends Document {
    description: String,
    canviewDashboard: { type: Boolean },
    canCreateUsers: { type: Boolean },
    canViewUsers: { type: Boolean },
    canUpdateUsers: { type: Boolean},
    canDeleteUsers: { type: Boolean},
    userId: { type: Schema.Types.ObjectId },
    
    markdelete: { type: Boolean },
    createdtime: { type: Date }

}

const PremissionSchema: Schema = new Schema({
    description: String,
    canviewDashboard: { type: Boolean, default: true },
    canCreateUsers: { type: Boolean, default: true },
    canViewUsers: { type: Boolean, default: true },
    canUpdateUsers: { type: Boolean, default: true },
    canDeleteUsers: { type: Boolean, default: true },
    createdTime: { type: Date, default: Date.now },
    markdelete: { type: Boolean }
},{ timestamps: true });

export default model<IPremission, PaginateModel<IPremission>>('premissions', PremissionSchema);
