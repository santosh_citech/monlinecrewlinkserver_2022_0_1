
import { Schema, model } from 'mongoose';
import { PaginateModel, Document } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

interface IRole extends Document {
    code: String,
    description: String,
    userId: { type: Schema.Types.ObjectId },
    privilegeCode:String,
    markdelete: { type: Boolean },
    createdtime: { type: Date }

}
const roleSchema: Schema = new Schema({
    code: { type: String, default: 'Guest' },
    description: { type: String, default: 'Only View Premission' },
    userId: { type: Schema.Types.ObjectId, ref: 'user' },
    markdelete: { type: Boolean },
    createdTime: { type: Date, default: Date.now }
},{ timestamps: true });

export default model<IRole, PaginateModel<IRole>>('role', roleSchema);

