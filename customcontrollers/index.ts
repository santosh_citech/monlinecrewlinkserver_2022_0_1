
import express from "express";
import morgan from 'morgan';
import UserController from "./user.controller"
//import LoginController from "./login.controller"
import RoleController from "./role.controller"
import UploadController from "./upload.controller";
const router = express.Router();
import multer from 'multer';
import moment from "moment";
import path from "path";

import * as fs from 'fs';
router.use(morgan('dev'));

const UPLOAD_PATH = './tempUploads';

if (!fs.existsSync(UPLOAD_PATH)) {
     fs.mkdirSync(UPLOAD_PATH);
}

var storage = multer.diskStorage({
     destination: function (req, file, cb) {
          cb(null, `${UPLOAD_PATH}`)
     },
     filename: function (req, file, cb) {
          cb(null, file.originalname + '_' + moment().format('YYYY-MM-DD'));
     }
})

var upload = multer({
     storage: storage,
     limits: { fileSize: 10 * 1024 * 1024 }, // 1MB
});



//app.use(upload.any());
//const loginController = new LoginController();
const userController = new UserController();
const roleController = new RoleController();
const uploadController = new UploadController();


/* GET home page. */
router.get('/', function (req, res, next) {
     // res.sendFile(__dirname + '/public/index.html')
     res.sendFile('index.html', { root: 'public' });
     // res.redirect('index');
});


router.get("/timeLog", function timeLog(req, res, next) {

     return res.send('Time: ' + moment().format('YYYY-MM-DD HH:mm:ss')).status(400);
     next();

});


//router.post('/login', loginController.login);
router.post('/uploadTrainDetils', upload.single('file'), uploadController.uploadTrainDetails);
router.post('/uploadStations', upload.single('file'), uploadController.uploadStations);
router.get('/api/v1/role/create', roleController.createRole);
router.get('/api/v1/user/create', userController.createUser);
router.get('/api/v1/user/getall', userController.getUsers);

export = router;