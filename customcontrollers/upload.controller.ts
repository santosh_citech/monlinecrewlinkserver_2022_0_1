
import { Request, Response, NextFunction } from 'express';

import * as fs from 'fs';
import * as moment from 'moment'
import path from "path";
import Upload from '../entities/upload.model';
import Trains from '../entities/train.model';

/**
 * @class utility
 */
export class Utility {
    constructor() { }
    private minutesToHours(minutes: any) {
        let mins: any = Math.round(parseInt((((minutes / 60) % 1).toFixed(2).substring(2))) * 0.6);
        mins = (mins < 10) ? "0" + mins : mins;
        let hrs: any = Math.floor(minutes / 60);
        hrs = (hrs < 10) ? "0" + hrs : hrs;
        return hrs + ":" + mins;
    }

    private hrsToMinutes(time: any): number {
        time = time.split(":");
        return parseInt(time[0]) * 60 + parseInt(time[1]);
    }

    private checkDuplicateFileUpload() {
        return;
    }
    protected getExtension(fname: any) {
        return fname.split('.').pop();

    }
    formatBytes(bytes: any, decimals: number = 2) {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + '' + sizes[i];
    }

}


export default class UploadController extends Utility {
    public filename?: string;
    public destination?: string;
    public fullpath?: string;
    public originalfilename?: string;
    public size?: number;
    public filetype?: string;
    public tempPath?: string;
    public csvData: [];
    public trainLists: any[] = [];
    public sizeInBytes: string;
    constructor() {
        super()
    }
    saveToUpload(sizeInBytes: any, csvdata: any) {

        new Upload({
            originalname: this.originalfilename,
            description: "lkkkk",
            creator: "san@123",
            filetype: sizeInBytes,
            status: true,
            comments: "uploaded successfully",
            fdata: csvdata,

        }).save().then(result => {

            console.log(result);
        }).catch(err => console.log('error: ', err));

    }

    private deletefile(fpath: string) {
        fs.unlink(path.join(fpath), (err) => {
            if (err) console.error(err);
        });

    }

    private WriteToFile(path: string, filedata: string) {
        let fullpath = ".\\" + path + "\\" + this.originalfilename;
        if (!path) {
            throw console.error("Path missing!!!");

        } if (path) {
            fs.writeFile(fullpath, filedata, "utf8", (err: any) => {
                (err)
                console.log(err);
                (!err)
                console.log("file has been written successfully !!!");
            });

        }

    }

    public uploadTrainDetails = async (req: Request, res: Response, next: NextFunction) => {
        if (!req.file) {
            return res.send('No files to upload.').status(400);
        }
        try {
            this.originalfilename = req.file?.originalname;
            this.destination = req.file?.destination;
            this.filename = req.file?.filename;
            this.tempPath = req.file?.path;
            this.fullpath = __dirname + "\\tempUploads\\" + this.filename;
            this.size = req.file?.size;
            this.sizeInBytes = this.formatBytes(req.file?.size);
            this.filetype = this.getExtension(req.file?.originalname);


            //     this.deletefile(this.tempPath);


            fs.readFile(path.resolve(this.tempPath), { encoding: 'utf-8' }, (err, csvdata) => {
                var tarr: any = [];
                var lines: any = [];
                var runningDaysArray: any = [];
                if (err) return console.error(err);
                if (!err) {
                    const allTextLines = csvdata.split(/\r?\n|\r/);
                    const headers = allTextLines[0].split(',');
                    for (var i = 1; i < allTextLines.length; i++) {
                        // split content based on comma
                        const data = allTextLines[i].split(',');

                        if (data.length === headers.length) {
                            tarr = [];
                            for (var j = 0; j < headers.length; j++) {
                                tarr.push(data[j]);
                            }
                        }
                        lines.push(tarr);
                    }

                }


                for (let item of lines) {
                    runningDaysArray = [];
                    for (var j = 0; j < 7; j++) {
                        var rd = item[4 + j];
                        if (rd != "") {
                            runningDaysArray.push(j);

                        }
                    }

                    this.createDataToArray(item[0], item[1], item[2], item[3], runningDaysArray, item[11])
                 }

                 const results = Trains.bulkCreate(this.trainLists);
                 console.log("results: ", results);

            });

        } catch (e) {
            return res.json(console.error(e));
        } finally {
            return res.json("File Uploaded Successfully!!!");
        }

    }

    async createDataToArray(col0: any, col1: any, col2: any, col3: any, col4: any, col5: any) {
        this.trainLists.push({
            "trainno": col0,
            "trainname": col1,
            "source": col2,
            "destination": col3,
            "traintype": col5,
            // "runningDays": col4,
        })
    }

    processDataToArray(data: any) {
        let csvToRowArray = [];
        let row = [];
        let header;
        let runningDaysArray: any[] = [];

        data += '\n';
        var re = /\r\n|\n\r|\n|\r/g;
        csvToRowArray = data.replace(re, "\n").split("\n");
        header = csvToRowArray[0];
        console.log(header);
        //data.length
        for (let index = 1; index < 101 - 1; index++) {

            runningDaysArray = [];
            row = csvToRowArray[index].split(",");


            for (var j = 0; j < 7; j++) {
                var runningDay = row[4 + j];
                if (runningDay != "") {
                    runningDaysArray.push(j);

                }
            }

            this.trainLists.push({
                "trainName": row[1],
                "trainNo": row[0],
                "fromStation": row[2],
                "toStation": row[3],
                "traintype": row[11],
                "runningdays": runningDaysArray
            })

        }
    }



    async uploadTrain(req: Request, res: Response, next: NextFunction) {


    }

    
    public uploadStations = async (req: Request, res: Response, next: NextFunction) => {
        if (!req.file) {
            return res.send('No files to upload.').status(400);
        }
        try {

        }catch(e){

        }
    }

}
