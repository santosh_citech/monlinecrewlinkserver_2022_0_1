
// tslint:disable-next-line: no-implicit-dependencies
import { Request, Response, NextFunction } from 'express';
import CRole from '../entities/role.model';
import * as fs from 'fs';
// tslint:disable-next-line: no-implicit-dependencies
import jwt from 'jsonwebtoken';
// tslint:disable-next-line: no-implicit-dependencies
require("dotenv").config();
require('process');
// tslint:disable-next-line: no-implicit-dependencies
import bcrypt from "bcrypt";

export default class RoleController {

    constructor() {



    }



    // tslint:disable-next-line: align
    async createRole(req: Request, res: Response, next: NextFunction) {
        const r = new CRole({
            "name": "admin",
            "description": " This role is assign for only administator of the panel"
        })
        r.save().then(result => {

            return res.json(result);
        }).catch(err => console.log('error: ', err));

    }

}
