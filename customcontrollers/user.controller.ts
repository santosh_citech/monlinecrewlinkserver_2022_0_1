import { Request, Response, NextFunction } from 'express';
import User from '../entities/user.model';
import bcrypt from "bcrypt";

export default class UserController {


    UserController() {

    }

    async createUser(req: Request, res: Response, next: NextFunction) {
        // const user = await User.create({
        //     firstname: "santosh",
        //     lastname: "sahu",
        //     email: "abc@gmail.com",
        //     password: "santosh123456"
        // });

        const hashedPassword = await bcrypt.hash("password", 10);
        const user = new User({
            firstname: "santosh",
            lastname: "sahu",
            email: "abc@gmail.com",
            token_id: 1,
            password: hashedPassword
        })

        user.save().then(r => {
            return res.status(201).send(r)
        }).catch(err => console.log('error: ' + err));

    };

    async getUsers(req: Request, res: Response, next: NextFunction) {


        await User.findAll().then(result => {
            return res.json(result);
        }).catch(err => console.log('error: ' + err));
    }

    // getting all posts
    getPosts(req: Request, res: Response, next: NextFunction) {
        return res.status(200).json({
            message: "hello"
        });
    };

}













