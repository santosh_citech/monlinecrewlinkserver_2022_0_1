import { Request, Response, NextFunction } from 'express';
import userModel from '../models/user.model';
import roleModel from '../models/role.model';
import q from 'q';
import nodemailer from "nodemailer";
import Mail from "nodemailer/lib/mailer";


export default class User {

    constructor() { }

    registerUser(req: Request, res: Response, next: NextFunction) {
        roleModel.create({
            code: req.body.code
        }, (err, result) => {

        })

        userModel.find({ username: req.body.username }, function (error, result) {
            if (error) {
                res.json(error);
            }
            else if (result.length === 0) {

                userModel.create({
                    username: req.body.username,
                    firstname: req.body.firstname,
                    lastname: req.body.lastname,
                    password: req.body.password,
                    email: req.body.email

                }, (error, result) => {
                    if (error) return error;
                    res.status(201);
                    res.json({
                        "result": true,
                        "status": "CREATED",
                        "message": "User created successfully"
                    });
                });


            }
            else {
                res.status(200);
                res.json({
                    "result": false,
                    "status": "EXISTS",
                    "message": "Username already exists. Please try a different username"
                });
            }




        });

    }

    async getUsers(req: Request, res: Response, next: NextFunction) {

        const query = {
            markdelete: false,
            // status: {
            //     $nin: ['deleted'],
            // },
        };

        let { page, limit, order }: any = req.query
        if (!page) {
            page = parseInt(page, 10) > 0 ? parseInt(page, 10) : 1;
        }
        if (!limit) {
            limit = parseInt(limit, 10) > 0 ? parseInt(limit, 10) : 10;
        }

        limit = parseInt(limit);
        page = parseInt(page);
         limit = parseInt(limit)
        const skip = (page - 1) * limit
        const previous_pages = page - 1;
        const result_documents: any = await userModel.find({}).limit(limit).skip(skip).lean();
        const result_documents_count = await userModel.countDocuments();
        const next_pages = Math.ceil((result_documents - skip) / limit);

        return res.status(200).json({
            "docs": result_documents,
            "totalDocs": result_documents_count,
            "page": page,
            "limit": limit,
            "nextPage": next_pages,
            "prevPage": previous_pages,
            "totalPages": Math.ceil((result_documents_count / limit))
        });


    }

    updateUser(req: Request, res: Response, next: NextFunction) {
        userModel.findByIdAndUpdate(req.body._id, { 'username': req.body.username, 'firstName': req.body.firstName, 'lastName': req.body.lastName, 'password': req.body.password, 'email': req.body.email, 'roleCode': req.body.roleCode }).then(function (result: any) {
            res.status(201);
            res.json({
                "status": 200,
                "message": "Update Successfully"
            });
        }, function (error: any) {
            console.log(error);
        });
    }

    deleteUser(req: Request, res: Response, next: NextFunction) {
        var id = req.params.id;
        userModel.findByIdAndUpdate(id, { markDelete: true }, (err: any, result: any) => {
            res.status(201);
            res.json({
                "status": 200,
                "message": "delete Successfully"
            });
        });
    }

    createnodemailer(email: string, passkey: string) {
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            port: 465,
            auth: {
                user: email,
                pass: passkey
            }
        });
        return transporter;
    }

    async resetUserPassword(req: Request, res: Response, next: NextFunction) {

        var from = "santosh.citech@gmail.com"

        if (!req.body.email) {
            return res
                .status(500)
                .json({ message: 'Email is required' });
        }
        const user = await userModel.findOne({
            email: req.body.email
        });
        if (!user) {
            return res
                .status(409)
                .json({ message: 'Email does not exist' });
        }

        // var trans = this.createnodemailer("santosh.citech@gmail.com", "1cd09mca13")

        var smtpTransport = nodemailer.createTransport({
            service: 'Gmail',
            port: 465,
            auth: {
                user: "santosh.citech@gmail.com",
                pass: "1cd09mca13"
            }
        });

        var mailOptions = {
            to: req.body.email,
            from: from,
            subject: 'Node.js Password Reset',
            text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                'http://localhost:4200/forgetpassword/forgetId' + '\n\n' +
                'If you did not request this, please ignore this email and your password will remain unchanged.\n'
        }


        smtpTransport.sendMail(mailOptions, (err, info) => {

            if (err) throw err
            else
                res.json("mail sent successfully !! " + info)
        })


    }


}



