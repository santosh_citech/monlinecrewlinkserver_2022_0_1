
import { Request, Response, NextFunction } from 'express';
import stationModel from '../models/station.model';

export default class StationController {

    constructor() {

    }

    getStations(req: Request, res: Response, next: NextFunction) {
        var options: any = {
            page: parseInt(req.query.page as string, 10) || 1,
            limit: parseInt(req.query.limit as string, 10) || 10,
            order: req.query.order || 'code',
            code: req.query.code,
            sort: { 'code': 1 },
            lean: true

        }

        // var query0 = stationModel.find({}).sort(options.order)
        var condition = options.code ? { code: { $regex: new RegExp(options.code), $options: "i" } } : {};

        stationModel.paginate(condition, options, function (err: any, result: any) {

            res.json(result).status(200)


        });






    }


    createBulkStations(data: any) {
        stationModel.insertMany(data);
    }

    deleteBulkStations(data: any) {
        stationModel.remove({ code: { $in: data } });
    }



}



