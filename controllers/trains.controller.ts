import { Request, Response, NextFunction } from 'express';
import trainModel from '../models/trains.model';
import q from "q";

export default class TrainController {

    constructor() {

    }

    async BulkInsertTrains(data: any) {
        const t = await trainModel.insertMany(data);
        if (t) return "Data Inserted Successfully in Trains Table!!"
    }


    async BulkDeleteTrains(fromstation: string) {
        const query = { source: new RegExp(fromstation, "i") };
        const result = await trainModel.deleteMany(query);
        var message = "Deleted " + result.deletedCount + " documents";
        return message;

    }
    getTrains(req: Request, res: Response, next: NextFunction) {
        var options: any = {
            page: parseInt(req.query.page as string, 10) || 1,
            limit: parseInt(req.query.limit as string, 10) || 10,
            order: req.query.order || 'trainname',
            trainno: parseInt(req.query.trainNumber as string, 10),
            trainname: req.query.trainname,
        }
        var orderby = options.order;
        var ascordsc = -1
        let obj: any = { $gte:options.trainno, $lte: options.trainno };

        var condition = options.trainno ? { trainno: obj } : {};

        trainModel.paginate(condition, options, function (err: any, result: any) {

            res.json(result).status(200)


        });


    }



    async deleteTrainsByRemove(data: any) {

        var response = await trainModel.remove({ trainNo: { $in: data } });
        console.log(response);
        return response;
    }


    async updateTrains(data: any) {
        var deferred = await trainModel.findByIdAndUpdate(data.id, { 'trainName': data.trainName, 'fromStation': data.fromStation, 'toStation': data.toStation, 'runningDays': data.runningDays, 'trainType': data.trainType });
        return deferred
    }


    doesTrainExist(data: any) {
        var deferred = q.defer();
        trainModel.find({ trainNo: data.trainNo }).then(function (result) {

            deferred.resolve({ originalData: data, result: result });

        }, function (error) {
            console.log(error);
        });
        return deferred.promise;

    }

}




