import { Request, Response, NextFunction } from 'express';
// import { userModel } from '../models/user.model';
import userModel from '../models/user.model';

import jwt from 'jsonwebtoken';
import config from '../config/config';
import express from 'express';
const app = express();
export class Authn {


    public name: string;
    constructor(name: string) {
        this.name = name;
        console.log(this.name);

    }
    async AuthenticationUser(username: string, password: string) {
        let dbUserObj: any = {}
        await userModel.find({ "username": username, "password": password }).then((result: any) => {
            if (result.length > 0) {
                dbUserObj = {
                    name: username,
                    role: result[0]._doc.rolecode,
                    username: username,
                };

            }

            else {
                dbUserObj = { role: undefined };

            }

        }).catch((err: any) => {
            throw new Error(err);

        });
        return dbUserObj;

    }



    generatorToken(userobj: any): any {
        var token = jwt.sign(userobj, config.secret, {
            expiresIn: 1440
        });
        return {
            token: token,
            userobj: userobj
        };
    };
}

export default class Authentication extends Authn {



    constructor() {
        super("call from base class in Authentication class ");
    }

    login(req: Request, res: Response, next: NextFunction) {
        var dbUserObj = {};
        var token:any;
        let { username, password } = req.body

        if ((username == '' || password === undefined) || (username == '' || password === undefined)) {
            return res.status(401).json({
                success: false,
                message: 'Please provide username & password!!!!'
            });

        }



        userModel.findOne({ "username": username }, (err: any, user: any, next: any) => {
            if (err) {
                throw err;
            }
            if (!user) {
                return res.json({ success: false, message: 'Authentication failed. User not found.' }).status(404);
            }
            if (user) {
                if (user.password != password) {
                    return res.json({ success: false, message: 'Authentication failed. Wrong password.' }).status(401);
                }
            }


            if (user) {
                dbUserObj = {
                    name: username,
                    role: user._doc.rolecode,
                    username: username,
                };
            }

            else {
                dbUserObj = { role: undefined };

            }


            let cookieoptions = {
                maxAge: 1000 * 60 * 15, // would expire after 15 minutes
                httpOnly: true, // The cookie only accessible by the web server
                signed: false, // Indicates if the cookie should be signed
                domain: 'wwww.monlinecrewlink.com',
            }
            token = super.generatorToken(dbUserObj);
            const currentDate = new Date();
            currentDate.setMinutes(currentDate.getMinutes() + 30);


            var cookie = req.cookies;
            delete cookie["login_user"];
            delete cookie["x-access-token"];
            delete cookie["x-key"];
            if (cookie === undefined || cookie === null || Object.keys(cookie).length === 0) {
                res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
                res.cookie('login_user', token.token, cookieoptions);
                res.cookie('x-access-token', token.token, { expires: currentDate });
                res.cookie('x-key', token.userobj.username);
                res.append('Set-Cookie', 'login_user=' + token.token + ';');
                return res.json({
                    success: true,
                    message: 'Auth granted, welcome!!!',
                    userobj: dbUserObj,
                    token: token
                });

            } else {
                console.error('cookie exists', cookie);
                return res.json({
                    success: true,
                    message: 'Auth granted, welcome!!!',
                    userobj: dbUserObj,
                    token: token
                });

            }

        })

    }


    logout(req: Request, res: Response) {
        let cookie = req.cookies;
        for (var prop in cookie) {
            if (!cookie.hasOwnProperty(prop)) {
                continue;
            }
            res.cookie(prop, '', { expires: new Date(0) });
        }
        res.clearCookie('x-access-token',{path: '/logout', domain: 'localhost'})
        res.clearCookie('x-key',{path: '/logout', domain: 'localhost'})
        res.clearCookie('login_user', { path: '/logout', domain: 'localhost' })

        delete cookie["login_user"];
        delete cookie["x-access-token"];
        delete cookie["x-key"];

      

         return res.json({message:'User Logged out successfully !!!!'})

    }
}

