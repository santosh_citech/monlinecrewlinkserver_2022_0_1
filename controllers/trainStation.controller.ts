
import { Request, Response, NextFunction } from 'express';
import trainStationModel from '../models/trainStation.model';
import { queryResolver } from "../library/queryResolver.library";

import q from 'q';

var qResolver = new queryResolver();
export default class TrainStation {

    constructor() {

    }



    findTrain(req: Request, res: Response, next: NextFunction) {
        var options = {
            limit: parseInt(req.query.limit as string, 10) || 10,
            page: parseInt(req.query.page as string, 10) || 1,
            order: req.query.order || 'stopNo',
        };
        var query;

        let trainno: any = req.query.trainno

        var obj = qResolver.getTrainNumberQuery(trainno);
        var condition = trainno ? { trainNo: obj } : {};

        trainStationModel.paginate(condition, options, function (err: any, response: any) {
            if (err) console.log(err);
            res.json(response);
        });
        // qResolver.resolveQuery(req.query, trainStationModel, options).then((response: any) => {
        //     res.json(response);
        // });
    }


    createTrainStation(data: any) {

        trainStationModel.insertMany(data);
    }

    deleteTrainStations(data: any) {

        trainStationModel.remove({ trainNo: { $in: data } });
    }



}



