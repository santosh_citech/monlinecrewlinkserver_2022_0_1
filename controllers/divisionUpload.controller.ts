import { Request, Response, NextFunction } from 'express';
import path from 'path';
import divisionModel  from '../models/division.model';
import Utility from '../utility/utilCal.utility';
import fs from 'fs';
import q from 'q';


export default class DivisionUpload {

    constructor() {

    }

    // async BulkDeleteStations(code: string) {
    //     const query = { 'code': new RegExp(code, "i") };
    //     const istrue = await stationModel.find(query);

    //     const result = await stationModel.deleteMany(query);
    //     var message = "Deleted " + result.deletedCount + " documents";
    //     return message;

    // }

    //  bulkDeleteFromStation(csvdata: any) {
    //     for (let data of csvdata) {
    //         BulkDeleteStations(data.source).then((result: string) => {
    //             if (result) console.log("" + result);

    //         });
    //     }
    // }
    async saveToDivisionTable(property: any) {
        if (property === null || property === "" || typeof property === "undefined") {
            return new Error("problem in data saving in division");
        }
        var r = await divisionModel.insertMany(property);
        if (r) {
            return "data saved successfully !!!"
        }

    }

    isEmpty(property: any) {
        return (property === null || property === "" || typeof property === "undefined");
    }

    processData(csvdata: any): any {
        var result: any[] = [];
        var obj: any = {};
        var allTextLines = csvdata.split(/\r\n|\n/);

        var headers = allTextLines[0].split(",");

        for (var i = 1; i < allTextLines.length - 1; i++) {
            obj = {};
            if (!(allTextLines[i] === undefined && allTextLines[i] === null && allTextLines[i] === "")) {
                var rows = allTextLines[i].split(",");
                var slNo = rows[0];
                var name = rows[1];

                obj["slNo"] = slNo;
                obj["name"] = name || "nil";
                result.push(obj);
            }
        }
        return result;
    }
    createDivUpload(req: Request, res: Response, next: NextFunction) {

        if (!req.file) {
            return res.send('No files to upload.').status(400);
        }

        try {

            var file = __dirname + "/" + req.file;
            var tempPath = req.file.path;


            fs.readFile(path.resolve(tempPath), { encoding: 'utf-8' }, (err: any, csvdata: any) => {
                if (err) {
                    console.log(err);
                }
                var r: any = this.processData(csvdata);
                Utility.convertToJson("division", r);
                // bulkDeleteFromStation(result);
                this.saveToDivisionTable(r).then((result) => {
                    console.log(result)

                });
                //  console.log(JSON.stringify(result));
                fs.unlink(path.join(tempPath), function (err) {
                    if (err) return console.error(err);
                    console.info('file deleted successfully');
                });


            });
        } catch (error) {

            console.log(error);

        }


    }



}


