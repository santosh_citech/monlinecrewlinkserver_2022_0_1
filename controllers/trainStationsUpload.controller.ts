import { Request, Response, NextFunction } from 'express';
import path from 'path';
import  trainStationModel  from '../models/trainStation.model';
import Utility from '../utility/utilCal.utility';
import timeCalc from '../utility/timeCalculator.utility';
import timeConverter from "../utility/timeConverter.utility"
import fs from 'fs';
import moment from 'moment';



export default class TrainStationUpload {

    constructor() {}

    async bulkDeleteFromTrainStation(data: any) {
        var message: string = "";
        var result: any = "";
        if (data != null || data != "undefined" || data != "") {
            for (let d of data) {
                const query = { 'stationCode': new RegExp(d.stationCode, "i") };
                const isData = await trainStationModel.find(query);
                if (isData.length > 0) {
                    result = await trainStationModel.deleteMany(query);
                    message = "Deleted " + result.deletedCount + " documents";
                } if (isData.length == 0) {
                    message = "Deleted " + result.deletedCount + " documents";
                }
            }
        }
        return message;
    }

     static async saveToTrainStation (data: any) {
        let message: string = "";
        if (data != null || data != "undefined" || data != "") {
            try {
                var isData = await trainStationModel.insertMany(data);
                if (isData.length > 0) {
                    message = "inserted documents successfully";
                }
            } catch (error) {
                message = "Error in inserted documents " + error + " failure";
            }
        }
        return message;
    }

    isEmpty(property: any) {
        return (property === null || property === "" || typeof property === "undefined");
    }

    createTrainStationUpload(req: Request, res: Response, next: NextFunction) {

        if (!req.file) {
            return res.send('No files to upload.').status(400);
        }

        try {
            var tempPath = req.file.path;
            var result: any[] = [];
            var obj: any = {};

            fs.readFile(path.resolve(tempPath), { encoding: 'utf-8' }, (err: any, csvdata: any) => {
                if (err) {
                    console.log(err);
                }
                else {
                    var rregExp = /\r\n|\n\r|\n|\r/g;
                    var allTextLines = csvdata.replace(rregExp, "\n").split("\n")

                    var headers = allTextLines[0].split(",");
                    var totaldistance: number = 0;
                    var totalArrivalTime: number = 0;
                    var count = 1;

                    for (let i = 1, len = allTextLines.length - 1; i < len; i++) {
                        obj = {};

                        if (!(allTextLines[i] === undefined && allTextLines[i] === null && allTextLines[i] === "")) {
                            var rows = allTextLines[i].split(",");

                            obj["trainNo"] = parseInt(rows[0]);
                            obj["stopNo"] = parseInt(rows[1]);
                            obj["stationCode"] = rows[2];
                            obj["dayOfJourney"] = parseInt(rows[3]);
                            obj["arrivalTime"] = rows[4];
                            obj["departureTime"] = rows[5];
                            obj["distance"] = parseInt(rows[6]);
                            obj["locotype"] = rows[7];
                            obj["arrivalday"] = (parseInt(rows[3]) - 1); // to calculate arrival day 
                            obj["departureday"] = obj["arrivalday"]; // to calcuate departure day 

                            var arrivalDateTimeObj = { day: obj["arrivalday"], time: obj["arrivalTime"] }
                            var departureDateTimeObj = { day: obj["departureday"], time: obj["departureTime"] }


                           // obj["arrivalInMinutes"] = timeCalc.convertDateTimeObjectToNumber(arrivalDateTimeObj, "minutes");;
                          //  obj["departureInMinutes"] = timeCalc.convertDateTimeObjectToNumber(departureDateTimeObj, "minutes");;


                            var departureDate: string;
                            var arrivalDate: string;

                            arrivalDate = obj["dayOfJourney"] + ' Jan 2012 ' + obj["arrivalTime"] + ':00 GMT+0000';

                            if (obj["arrivalInMinutes"] > obj["departureInMinutes"]) {
                                obj["departureDay"] = parseInt(obj["arrivalday"]) + 1;
                                departureDate = (obj["dayOfJourney"] + 1) + ' Jan 2012 ' + obj["departureTime"] + ':00 GMT+0000';
                            }
                            else {
                                obj["departureDay"] = obj["arrivalday"];
                                departureDate = (obj["dayOfJourney"]) + ' Jan 2012 ' + obj["departureTime"] + ':00 GMT+0000';
                            }
                            var arrivalDateTime = new Date(arrivalDate);
                            var departureDateTime = new Date(departureDate);
                            obj["arrivalDateTime"] = arrivalDateTime;
                            obj["departureDateTime"] = departureDateTime;

                           // totaldistance = timeConverter.calculateTotalDistance(obj["distance"]);
                           // totalArrivalTime = timeConverter.calculateSumOfTime(obj["arrivalTime"]);


                            /*
                               logic for calculate halt number its works as stop number 
                               because in UI if we select a rows halt/stop number is required   
                             */

                            if ((obj["arrivalInMinutes"] < obj["departureInMinutes"])) {
                                obj["haltNo"] = count;
                                count++;
                            }
                            else {
                                obj["haltNo"] = count;
                                obj["totaldistance"] = totaldistance;
                                obj["totalArrivalTime"] = totalArrivalTime;
                                count = 1;
                            }
                            result.push(obj);


                        }


                    }




                    // Utility.convertToJson("trainstation", result);


                    // bulkDeleteFromTrainStation(result).then((successresult) => {

                    // });

                    var endtime: any;
                   // var starttime: any = timeConverter.calculateExecutionTime(new Date().getTime());
                   TrainStationUpload.saveToTrainStation(result).then((ressuccess: any) => {

                      //  endtime = timeConverter.calculateExecutionTime(new Date().getTime());
                      //  console.log(timeConverter.CalDifferecebetweenTwoTimes(starttime, endtime));
                        res.status(201);
                        return res.json({
                            "message": ressuccess
                        })
                    })






                    fs.unlink(path.join(tempPath), function (err) {
                        if (err) return console.error(err);

                    });

                }
            });
        } catch (error) {

            console.log(error);

        }
    }


}


