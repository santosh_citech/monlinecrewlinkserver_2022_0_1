

import { Request, Response, NextFunction } from 'express';
import { planModel } from '../models/plan.model';

import q from "q";


export default class Plans {

    constructor() {

    }


    getUserPlan(req: Request, res: Response, next: NextFunction) {
        // const postCount = req.posts.length;

        var pageOptions: any = {
            page: parseInt(req.query.page as string, 10) || 1,
            limit: parseInt(req.query.limit as string, 10) || 10,
            order: req.query.order || 'planName'
        }

        var query = planModel.find({ markDeleted: false }).sort({ planName: 'asc' }).skip((pageOptions.page * pageOptions.limit)).limit(pageOptions.limit)
        query.exec((err: any, data: any) => {
            if (err) res.status(500).json({
                msg: "Error en aplicacion",
                err
            });
            return res.status(200).json(data);
        });
    }

    setUserPlan(data: any) {
        var deferred = q.defer();
        planModel.findByIdAndUpdate(data, { 'isLinkGenerated': true }, (err, result) => {
            if (err) {
                if (err) throw err;
            }
            deferred.resolve(result);
        });
        return deferred.promise;
    }


    savePlan(data: any, res: Response) {
        planModel.create({ planName: data.planName, owner: data.owner, coPlanners: data.coPlanners }, (err, result) => {
            if (err) return err;
            else {
                res.json(result);
            }
        });
    }


    createPlan(req: Request, res: Response, next: NextFunction) {

        planModel.create({ planName: req.body.planName, owner: req.body.owner }, (err, result) => {
            if (err) {

                res.status(404);
                res.json({
                    "message": "plan can't created ",
                    "stats": "failure",
                    "error": err
                })
                console.log("Error" + err);
            }
            res.status(201);
            res.json({
                "message": "santosh001",
                "stats": "success"
            })
        });
    }

    getOnePlan(req: Request, res: Response, next: NextFunction) {
        var options = {
            perPage: req.query.limit || 10,
            page: req.query.page || 1,
            order: req.query.order || 'planName'

        };
        var query;

    }



    deletePlan(req: Request, res: Response, next: NextFunction) {
        var id = req.params.id;
        planModel.findByIdAndUpdate(id, { 'markDelete': true }, (error, result) => {
            res.status(201);
            res.json({
                "status": 200,
                "message": "Delete Plan Successfully"
            });

        })

    }

    createCoPlan(req: Request, res: Response, next: NextFunction) {

    }

    updateReviewer(req: Request, res: Response, next: NextFunction) {

    }

    copyPlan(req: Request, res: Response, next: NextFunction) {

    }

    mergePlan(req: Request, res: Response, next: NextFunction) {

    }

}




