

import { Request, Response, NextFunction } from 'express';

import path from 'path';

import stationModel  from '../models/station.model';
import Utility from '../utility/utilCal.utility';
import fs from 'fs';




export default class StationUpload {

    constructor() { }

    async BulkDeleteStations(code: string) {
        const query = { 'code': new RegExp(code, "i") };
        const istrue = await stationModel.find(query);

        const result = await stationModel.deleteMany(query);
        var message = "Deleted " + result.deletedCount + " documents";
        return message;

    }

    bulkDeleteFromStation(csvdata: any) {
        for (let data of csvdata) {
            this.BulkDeleteStations(data.source).then((result: string) => {
                if (result) console.log("" + result);

            });
        }
    }
    async saveToStation(data: any) {

        var result = await stationModel.insertMany(data);

    }

    isEmpty(property: any) {
        return (property === null || property === "" || typeof property === "undefined");
    }

    createStationUpload(req: Request, res: Response, next: NextFunction) {

        if (!req.file) {
            return res.send('No files to upload.').status(400);
        }

        try {
            let fileName: any = req.file.originalname;
            var file = __dirname + "/" + req.file;
            var tempPath = req.file.path;
            var result: any[] = [];
            var obj: any = {};

            fs.readFile(path.resolve(tempPath), { encoding: 'utf-8' }, (err: any, csvdata: any) => {
                if (err) {
                    console.log(err);
                }
                else {

                    var allTextLines = csvdata.split(/\r\n|\n/);

                    var headers = allTextLines[0].split(",");

                    for (var i = 1; i < allTextLines.length - 1; i++) {
                        obj = {};
                        if (!(allTextLines[i] === undefined && allTextLines[i] === null && allTextLines[i] === "")) {
                            var rows = allTextLines[i].split(",");
                            var slNo = rows[0];
                            var code = rows[1];
                            var headstationsignOnTime = rows[2];
                            var headstationsignoffTime = rows[3];
                            var name = rows[4];
                            var noofBeds = rows[5];
                            var outStationSignOnTime = rows[6];
                            var outStationSignOffTime = rows[7];

                            obj["slNo"] = slNo;
                            obj["code"] = code;
                            obj["headstationsignOnTime"] = headstationsignOnTime || 30;
                            obj["headstationsignoffTime"] = headstationsignoffTime || 30;
                            obj["name"] = name || "nil";
                            obj["noofBeds"] = noofBeds || 1;
                            obj["outStationSignOnTime"] = outStationSignOnTime || 30;
                            obj["outStationSignOffTime"] = outStationSignOffTime || 30;


                            result.push(obj);
                        }


                    }

                    Utility.convertToJson("station", result);
                    this.bulkDeleteFromStation(result);
                    // saveToStation(result).then((res) => {

                    // }).catch((error) => {

                    // });
                    //  console.log(JSON.stringify(result));
                    fs.unlink(path.join(tempPath), function (err) {
                        if (err) return console.error(err);
                        console.info('file deleted successfully');
                    });

                }
            });
        } catch (error) {

            console.log(error);

        }


    }

}

