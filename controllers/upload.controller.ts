
import express from 'express';
import { Request, Response, NextFunction } from 'express';
import multer from 'multer';
import path from 'path';
var upload = multer();
var router = express.Router();
import { uploadModel } from '../models/upload.model';
import Q from 'q';


// var timeCal = from '../lib/timeCal.js');


///var utilCal = from "../utility/utilCal.js");



export default class Uploading {

    constructor() {

    }

    getUploads(req: Request, res: Response, next: NextFunction) {
        var query8 = {
            markDelete: false
        }
        var prj = [
            "_id",
            "originalFileName",
            "dataType",
            "fileType",
            "creator",
            "uploadedBy",
            "isProcessed",
            "status"
        ]

        var qr = uploadModel.find(query8).select(prj)
        qr.exec((err: any, data: any) => {
            if (err) {
                res.status(500).json({
                    msg: "Error en application",
                    err
                });
                throw Error("Error " + err)

            }
            if (!err) {
                return res.status(200).json({
                    "message": "user listings retrieved successfully!",
                    "result": data,

                });

            }
        });
    }





    getOneUpload(req: Request, res: Response, next: NextFunction) {


    }











    UploadData(data: any, dataType: any, fileType: any, originalFileName: any, uploadedBy: any) {
        let ddata: any;
        let ddataType: any;
        let dfileType: any;
        let doriginalFileName: any;
        let duploadedBy: any;

        ddata = data;
        ddataType = dataType;
        dfileType = fileType;
        doriginalFileName = originalFileName;
        duploadedBy = uploadedBy;
    }

    deleteUpload(data: any) {
        var _id = data.params.id;
        var deferred = Q.defer();
        uploadModel.findByIdAndUpdate(_id, { 'markDelete': true }, { new: true }, (error: any, result: any) => {
            if (error) return error;
            deferred.resolve(result);
        })
        return deferred.promise;
    }

    updateUpload(data: any) {
        var deferredUpdate = Q.defer();
        uploadModel.findByIdAndUpdate(data, { 'isProcessed': true, 'status': "processed" }, (error: any, result: any) => {
            if (error) return error;
            deferredUpdate.resolve(result);
        })
        return deferredUpdate.promise;
    }

}






