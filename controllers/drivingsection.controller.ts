
export class DrivingSectionController {
    getAll() {
        return 'This action returns all users';
    }

    getOne( id: number) {
        return 'This action returns user #' + id;
    }

    
    post(user: any) {
        return 'Saving user...';
    }

 
    put() {
        return 'Updating a user...';
    }

   
    remove() {
        return 'Removing user...';
    }
}