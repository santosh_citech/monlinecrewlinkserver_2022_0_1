
import divisionModel from '../models/division.model';
import { Request, Response, NextFunction } from 'express';

export default class DivisionController {

    constructor() {

    }



    getdivisions(req: Request, res: Response, next: NextFunction) {
        var options: any = {
            page: parseInt(req.query.page as string, 10) || 1,
            limit: parseInt(req.query.limit as string, 10) || 10,
            order: req.query.order || 'name',
            name: req.query.name,
            sort: { name: -1, slNo: -1 },
            lean: true,
           
        }

        // var query0 = stationModel.find({}).sort(options.order)
        var condition = options.name ? { name: { $regex: new RegExp(options.name), $options: "i" } } : {};

        divisionModel.paginate(condition, options).then((data) => {

            res.json(data).status(200)


        }).catch((err) => {
            // logger.error(req.method + ": " + req.originalUrl + ", message: " + err.message)
            throw Error(req.method + ": " + req.originalUrl + ", message: " + err.message)

        });






    }




}


