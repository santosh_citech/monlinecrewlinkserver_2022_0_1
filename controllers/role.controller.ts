import { Request, Response, NextFunction } from 'express';
import roleModel from '../models/role.model';
import userModel from '../models/user.model';
export default class RoleController {



    constructor() {

    }


    // isRoleExist(code: string) {

    //     userModel.findOne({ code: code }, (error: string, result: string | any[]) => {
    //         if (error) {
    //             throw Error("Error is isRoleExist() " + error)
    //         }
    //         if (result.length > 1) {
    //             return true;
    //         }

    //     });

    // }

    async createAndAssignRole(req: Request, res: Response, next: NextFunction) {
        let { role, roleType, userinfo }: any = req.body;
        if (Object.keys(req.body).length === 0)
            return res.status(400).send("Your request is missing details.");
        RoleController.findUser(userinfo.username).then((user) => {
            let userId = user._doc._id
            const data = { userId: userId, code: roleType };
            roleModel.create(data, function (err: any, result: any) {
                if (err) return err;
                else {
                    res.status(201);
                    return res.json({
                        success: true,
                        message: 'role created successfully !!!',

                    });
                }
            });
        });
    }

    async findRoleWithSelectedColoumn(req: Request, res: Response, next: NextFunction) {
        const query = {
            creator: "",
            status: {
                $nin: ['deleted'],
            },
        };


        try {
            let { page, size }: any = req.query
            if (!page) {
                page = parseInt(page, 10) > 0 ? parseInt(page, 10) : 1;
            }
            if (!size) {
                size = parseInt(size, 10) > 0 ? parseInt(size, 10) : 10;
            }
            const limit = parseInt(size)
            const skip = (page - 1) * size
            const result_documents: any = await roleModel.find({}).limit(limit).skip(skip).select("code").populate('userId').lean();

            return res.json({
                docs: result_documents,

            });
        } catch (error) {
            console.log("error", error)
            return res.json({ "error": error })
        }

    }
    async getroles(req: Request, res: Response, next: NextFunction) {

        const query = {
            creator: "",
            status: {
                $nin: ['deleted'],
            },
        };

        try {
            let { page, size, code }: any = req.query
            if (!page) {
                page = parseInt(page, 10) > 0 ? parseInt(page, 10) : 1;
            }
            if (!size) {
                size = parseInt(size, 10) > 0 ? parseInt(size, 10) : 10;
            }
            const limit = parseInt(size)
            const skip = (page - 1) * size
            const previous_pages = page - 1;

            var condition = code ? { "code": { $regex: new RegExp(code), $options: "i" } } : {};

            const result_documents: any = await roleModel.find(condition).limit(limit).skip(skip).populate('userId').lean();

            const next_pages = Math.ceil((result_documents - skip) / size);
            const result_documents_count = await roleModel.countDocuments();

            return res.json({
                page: page,
                limit: size,
                previous: previous_pages,
                next: next_pages,
                totalDocs: result_documents_count,
                docs: result_documents,

            });
        } catch (error) {
            console.log("error", error)
            return res.json({ "error": error })
        }

    }
    static async findUser(username: any) {
        try {
            let user: any = await userModel.findOne({ username: username }).exec();
            if (user === undefined) {
                return 'findUser() method undefined in roleController';
            } else {
                return user;
            }

        } catch (err) {
            return 'error occured in findUser() of role Controller';
        }

    }

    createRole(req: Request, res: Response, next: NextFunction) {
        if (req.query.username === undefined)
            return res.status(400).send("please provide username");

        let { username } = req.query;
        if (Object.keys(req.body).length === 0)
            return res.status(400).send("Your request is missing details.");

        if (req.body.code === undefined)
            return res.status(400).send("please provide role code");

        RoleController.findUser(username).then((user) => {
            let userId = user._doc._id
            const data = { userId: userId, code: req.body.code };
            roleModel.create(data, function (err: any, result: any) {
                if (err) return err;
                else {
                    res.status(201);
                    return res.json({
                        success: true,
                        status:"ok",
                        message: 'role created !!!',

                    });
                }
            });

        })

    }

}

