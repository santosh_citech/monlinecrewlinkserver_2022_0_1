
import express from 'express';
import { Request, Response, NextFunction } from 'express';
import multer from 'multer';
import path from 'path';
var upload = multer();
var router = express.Router();
import Q from 'q';
import { uploadModel } from '../models/upload.model';
import fs from 'fs';
const fsPromises = fs.promises;
import TrainController from './trains.controller';
var traincontroller = new TrainController();


export default class TrainUploadController {

    constructor() {

    }


    saveUpload(sizeInBytes: any, csvdata: any, originalfilename: string) {

        uploadModel.create({
            originalname: originalfilename,
            description: "lkkkk",
            creator: "san@123",
            fileType: sizeInBytes,
            status: "kmjj",
            comments: "uploaded successfully",
            fdata: csvdata

        }, function (err, post) {
            if (err) throw err;
            return ({
                "result": true,
                "status": "CREATED",
                "message": "uploaded created successfully"
            });
        });
    }

    static bulkDeleteFromTrain(csvdata: any) {
        for (let data of csvdata) {
            traincontroller.BulkDeleteTrains(data.source).then(function (result: string) {
                if (result) console.log("" + result);

            });
        }
    }

    static saveDataToTable(csvdata: any[]) {
        if (!csvdata || typeof (csvdata) === "undefined" && csvdata !== null) console.log("data is null ");
        try {
            traincontroller.BulkInsertTrains(csvdata).then((data) => {
                if (data) console.error("" + data);
            });
        } catch (err) {
            console.error(err);
        }

    }




    static processData(allText: string) {
        var allTextLines = allText.split(new RegExp(/\r\n|\n/));
        var headers = allTextLines[0].split(',');
        var runningdays: any = [];
        var tarr: any = [];
        for (var j = 1; j < allTextLines.length - 1; j++) {
            var rows = allTextLines[j].split(',');
            runningdays = [];
            for (var k = 0; k < 7; k++) {

                if (rows[k + 4] == "y" || rows[k + 4] == "Y") {
                    runningdays.push(k);
                }

            }

            tarr.push({
                "trainno": parseInt(rows[0]),
                "trainname": rows[1],
                "source": rows[2],
                "destination": rows[3],
                "runningDays": runningdays,
                "trainType": rows[11],
            });


        }
        // this.bulkDeleteFromTrain(tarr);

        return tarr;

    }

    /**
   * Fetch from the swapi API
   *
   * @param {string} tempPath
   * @param {string} file
   * @returns {string} JSON
   */
    async unlinkFile(tempPath: string, file: string): Promise<void> {
        const response = fs.unlinkSync(path.join(tempPath))
        console.log(response);
        return response;
    }

    createTrainUpload(req: Request, res: Response, next: NextFunction) {

        if (!req.file) {
            return res.send('No files to upload.').status(400);
        }

        try {
            let fileName: any = req.file.originalname;
            var file = __dirname + "/" + req.file;
            var tempPath = req.file.path;

            fs.readFile(path.resolve(tempPath), { encoding: 'utf-8' }, (err: any, csvdata: any) => {
                if (err) {
                    console.log(err);
                }
                else {
                    // var fname = GetFileName(fileName, "method1");

                    //   newUploads.saveDataToTable(data);
                    // newUploads.WriteToJsonFile(fname, data);

                    // utilCal.convertToPDF(data, fname);

                    var data = TrainUploadController.processData(csvdata);
                   // TrainUploadController.saveDataToTable(data);
                   
                   uploadModel.create({
                    originalFileName: fileName,
                    description: "lkkkk",
                    creator: "san@123",
                    fileType: "5kb",
                    status: "kmjj",
                    comments: "uploaded successfully",
                    fdata: csvdata
        
                }, function (err, post) {
                    if (err) throw err;
                    return ({
                        "result": true,
                        "status": "CREATED",
                        "message": "uploaded created successfully"
                    });
                });

                }
            });
        } catch (error) {
            console.log(error);
        }
    }



    //  pushDataToArray(trainNo: any, trainName: any, fromStation: any, toStation: any, runningDays: any, trainType: any, locoType: any) {
    //     trainListArray.push({
    //         "trainNo": trainNo,
    //         "trainName": trainName,
    //         "fromStation": fromStation,
    //         "toStation": toStation,
    //         "runningDays": runningDays,
    //         "trainType": trainType,
    //         "locoType": locoType

    //     })
    // }

}
