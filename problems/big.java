/* package codechef; // don't place package name! */

import java.util.*;

import java.io.*;

/***
 * 
 * You're given a number N. If N is divisible by 5 or 11 but not both then print
 * "ONE"(without quotes). If N is divisible by both 5 and 11 then print
 * "BOTH"(without quotes). If N is not divisible by 5 or 11 then print
 * "NONE"(without quotes).
 * /
 * 
 * 
 * 
 * /* Name of the class has to be "Main" only if the class is public.
 */
class Codechef {
    public static void main (String[] args) throws java.lang.Exception
	{
	try{
    
    System.out.println("Enter the Number");
        Scanner n = new Scanner(System.in);
        
            String my_str = n.nextLine();
              int m = Integer.parseInt(my_str);
              
              
             if((m%5==0)&&(m%11==0)){
                   System.out.println("BOTH");
              }
              else if(m%5==0||m%11==0){
                  System.out.println("ONE");
                  
              }
              else{
                    System.out.println("NONE");
              }
        
      
		
	}catch(e){
        System.out.print(e);
    }
}
}