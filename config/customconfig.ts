import dotenv from "dotenv";
dotenv.config();

/**
 *  Server configuration 
 */
const SERVER_PORT = process.env.SERVER_PORT || 1337;
const SERVER_HOSTNAME = process.env.SERVER_HOSTNAME || 'localhost';
const SERVER = {
    hostname: SERVER_HOSTNAME,
    port: SERVER_PORT
}

/**
 * Database server configuration
 */
const DB_SERVER_PORT = process.env.DB_PORT || 1337;
const DB_SERVER_HOSTNAME = process.env.DB_HOSTNAME || 'localhost';
const DB_SERVER_DATABASE = process.env.DB_DATABASE || '';
const DB_SERVER_USERNAME = process.env.DB_USERNAME || '';
const DB_SERVER_PASSWORD = process.env.DB_PASSWORD || '';
const DB_SERVER_DIALECT = process.env.DB_DIALECT || '';

const DBSERVER = {
    hostname: DB_SERVER_HOSTNAME,
    port: DB_SERVER_PORT,
    database: DB_SERVER_DATABASE,
    username: DB_SERVER_USERNAME,
    password: DB_SERVER_PASSWORD,
    dialect: DB_SERVER_DIALECT

}

const config = {
    server: SERVER,
    dbserver: DBSERVER
}
export default config;