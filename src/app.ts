import express from 'express';
import { Request, Response, NextFunction } from 'express'
import http from "http";
import * as dotenv from "dotenv";
import helmet from "helmet";
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import routes from '../routes/index'
import cors from "cors";
dotenv.config();
import config from '../config/config';
import DatabaseConnection from '../database/database';
import timecalc from "../utility/timeCalculator.utility"
import timeConverter from "../utility/timeConverter.utility"
import * as UtilityConverter from '../utility/utilCal.utility'

import { DrivingSectionController } from '../controllers/drivingsection.controller';
import secret from '../config/secret';

new DatabaseConnection();

var corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // For legacy browser support
}


if (!process.env.PORT) {
    process.exit(1);
}

const app = express();

app.set('superSecret', secret.secretKey); // secret variable

app.use(morgan('dev'));
app.use(helmet());
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));
app.use(cookieParser());

app.use(cors(corsOptions))

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Expose-Headers", "x-total-count");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
    res.header("Access-Control-Allow-Headers", "Content-Type,authorization");

    next();
});



/** Error handling */
app.use((error: any, req: Request, res: Response, next: NextFunction) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message,
        },
    });
});





app.use("/", routes)

const port: number = parseInt(process.env.PORT as string, 10) || 7000;
app.set('port', port);
const server = http.createServer(app);


(async () => {
    server.listen(port, () => {
        console.log(`⚡️[server]: Server is running at https://localhost:${port}`);
    });
   // var io = require('socket.io').listen(server);
    //require('../utility/chat')(app, io);

})();




var from = { day: 1, time: "05:53" };
var to = { day: 1, time: "14:59" };

//console.log(timecalc.convertDateTimeObjectToNumber(from, "hrs"));
//console.log(timecalc.getTimeFromMins("192.25"));
const arrivalTimeobj = { time: "02:04:00", day: 1, option: "minutes" }
//new timeConverter().calculateTimeInToNumber(arrivalTimeobj);



// var a = ["23:22:00", "1:00:00", "3:15:00", "8:15:00"]
// var s: any;
// for (var i = 0, len = a.length; i < len; i++) {
//     s = timeConverter.calculateSumOfTime(a[i]);
// }
// console.error(s);
// console.log(new timeConverter().minsToHHMMSS(s));

///console.log(timeConverter.relativeTime("12:00:30"));


export { app };