
import mongoose from "mongoose";
import { ConnectOptions } from 'mongoose';
import * as dotenv from "dotenv";
dotenv.config();
export default class DatabaseConnection {

    constructor() {
        this.setupDb();
    }

    private setupDb() {
        var uri = 'mongodb://localhost:27017/crewlink';
        mongoose.Promise = global.Promise;
        mongoose.connect(uri, {
            useNewUrlParser: !0,
            useUnifiedTopology: !0,
        } as ConnectOptions);
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB Connection error'));
        db.once('open', () => {
            console.log('Database Connection Successfully!!!');
        });
      
    }
}