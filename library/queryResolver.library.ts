var q = require('q');

export class queryResolver {
    gQuery: any;
    gObjectArray: any = [];
    nonDBFieldsArray: any = ['limit', 'page', 'order'];

    constructor() {

    }

    resolveQuery(queryObject: any, ModelClass: any, options: any) {

        try {
            var deferred = q.defer();
            this.gObjectArray = [];
            // this.gQuery = this.processFields(null, queryObject, ModelClass, options);
            var obj = this.getTrainNumberQuery(queryObject.trainno);
            var condition = queryObject.trainno ? { trainNo: obj } : {};

            ModelClass.paginate({}, options, function (err: any, response: any) {
                if (err) console.log(err);
                deferred.resolve(response);
            });
            return deferred.promise;

        } catch (Error: any) {
            console.log("Error inside resolveQuery() methods " + Error);
        }

    }

    processFields(trainNumbers: any, queryObject: any, ModelClass: any, options: any) {

        try {
            for (var qr in queryObject) {
                if (queryObject[qr] != '' && this.nonDBFieldsArray.indexOf(qr) === -1) {
                    if (qr === 'trainNo') {
                        var q = {};
                        q[qr] = this.getTrainNumberQuery(queryObject[qr]);

                        this.gObjectArray.push(q);
                    } else {
                        var q = {};
                        q[qr] = { $regex: queryObject[qr], $options: 'i' };
                        this.gObjectArray.push(q);
                    }
                }
            }


            if (this.gObjectArray.length == 0) {
                this.gQuery = ModelClass.find({}).sort(options.order);
            }
            else if (this.gObjectArray.length > 0) {
                this.gQuery = ModelClass.find({}).and([this.gObjectArray[0]]).sort(options.order);
                for (var i = 1; i < this.gObjectArray.length; i++) {
                    this.gQuery._conditions.$and.push(this.gObjectArray[i]);
                }
            }



        }
        catch (Error) {
            console.log("Error inside processFields() methods " + Error);
        }
        return this.gQuery;


    }

    getTrainNumberQuery(trainno: number) {
        try {

            const trainNumber = trainno.toLocaleString();
            var toTrainNumber = trainNumber;
            var fromTrainNumber = trainNumber;
            var len = 5 - trainNumber.length;
            for (let j = 0; j < len; j++) {
                fromTrainNumber += '0';
                toTrainNumber += '9';
            }

            // let obj: any = { $gte: fromTrainNumber, $lte: toTrainNumber };
            let obj: any = { $gte: parseInt(fromTrainNumber), $lte: parseInt(toTrainNumber) };
            return obj;
        } catch (err) {
            console.error("Error inside getTrainNumberQuery() methods "+err);
        }




    }

}