import express from 'express'
import { Request, Response, NextFunction } from 'express'
import multer from 'multer';
var upload = multer({ dest: './uploads' });
import Authentication from "../controllers/auth.controller"
import RoleController from '../controllers/role.controller';


import Uploading from '../controllers/upload.controller';
import TrainUploadController from '../controllers/trainUpload.controller';

import StationUpload from '../controllers/stationsUpload.controller';
import DivisionUpload from '../controllers/divisionUpload.controller';
import TrainStationUpload from '../controllers/trainStationsUpload.controller';

import PremissionController from '../controllers/premission.controller';
import Plans from '../controllers/plans.controller';

import TrainController from '../controllers/trains.controller';
import trainstation from '../controllers/trainStation.controller';
import User from '../controllers/user.controller';
import { verifyToken } from '../middlewares/authmiddle';

// station

import StationController from "../controllers/stations.contoller";

import divisionController from "../controllers/division.controller";

const router = express.Router();
var auth = new Authentication();

// const isLoggedIn = (req: Request, res: Response, next: NextFunction) => {
//     if (req.isAuthenticated()) {
//       req.session.isAuthenticated = true;
//       res.locals.isAuthenticated = true;
//       res.locals.user =req.user; 
//       next(); //If you are authenticated, run the next
//     } else {
//       return res.redirect("/login");
//     }
//   }


router.get('/', verifyToken, function (req: Request, res: Response, next: NextFunction) {
    return res.json(req.cookies);
})


router.post('/login', auth.login);

router.post('/logout', auth.logout);
router.post('/register', new User().registerUser);


// all roles
router.post('/api/v1/role', new RoleController().createRole);
router.get('/api/v1/role/getroles', new RoleController().getroles);
router.get('/api/v1/role/findRoleWithSelectedColoumn', new RoleController().findRoleWithSelectedColoumn);
router.post('/api/v1/role/createrole', new RoleController().createAndAssignRole);


router.post('/api/v1/privilage', new PremissionController().savePremission);



router.get('/api/v1/Upload', new Uploading().getUploads);
router.post('/api/v1/Upload/delete/:id', new Uploading().deleteUpload);
router.get('/api/v1/Upload/getone/:id', new Uploading().getOneUpload);



// upload train details 
router.post('/api/v1/trainupload', upload.single('file'), new TrainUploadController().createTrainUpload);


//router.post('/api/v1/trainStationUpload/', upload.single('file'), new TrainStationUpload().createTrainStationUpload);



//router.post('/api/v1/stationUpload/', upload.single('file'), new StationUpload().createStationUpload);


//router.post('/api/v1/divisionUpload/', upload.single('file'), new DivisionUpload().createDivUpload);

router.get('/api/v1/admin/users', new User().getUsers);
router.put('/api/v1/admin/user/', new User().updateUser);
router.put('/api/v1/admin/user/:id', new User().deleteUser);
router.post('/api/v1/admin/resetpassword', new User().resetUserPassword);



/*
 * Routes for Access UserPlan 
 */
router.get('/api/v1/getplans', new Plans().getUserPlan);
router.get('/api/v1/plans/getOnePlan', new Plans().getOnePlan);
router.post('/api/v1/createPlan', new Plans().createPlan);
router.put('/api/v1/createCoPlan/:id', new Plans().createCoPlan);
router.put('/api/v1/plan/updateReviewer/:id', new Plans().updateReviewer);
router.put('/api/v1/plan/deletePlan/:id', new Plans().deletePlan);
router.post('/api/v1/copyPlan', new Plans().copyPlan);
router.post('/api/v1/mergePlan', new Plans().mergePlan);


/// trains 
router.get('/api/v1/gettrains', new TrainController().getTrains);

// trainstation 
router.get('/api/v1/gettrainstations', new trainstation().findTrain);

// stations

router.get('/api/v1/getstations', new StationController().getStations);
//divisions
router.get('/api/v1/getdivisions', new divisionController().getdivisions);

import {paginationResolver} from "../library/pagination.library"

import isAuth  from "../middlewares/isAuthentication";
router.get('/api/v1/searched',isAuth.isAuthenticationFun,new paginationResolver().searchedTrains);

export default router